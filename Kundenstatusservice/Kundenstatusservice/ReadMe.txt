--- Kundenstatusservice ---

Usage:
	-uri <Base-URI>

Beispiel:
	-uri http://localhost:8080/rest

Erklärung:
	Der Kundenstatusservice stellt unter dem angegebenen Uri einen Webservice zur Kundenstatusverwaltung zur Verfügung.
	Json-Beispiele finden sich unter https://abmsoft.atlassian.net/wiki/spaces/KOB/pages/1454538757/Schnittstellen+-+JSON+Beispiele .
	
Requests:

	Get-Request an <Base-URI>/supermarket/status
		Antwortet mit dem aktuellen Marktstatus über alle Kunden
	
	Put-Request an <Base-URI>/supermarket/newcustomer/<ID>
		Einspeichern des berechneten Pfades für einen neuen Kunden, ID ist die id des einzuspeichernden Kunden
		
	Get-Request an <Base-URI>/supermarket/customerpath/<ID>
		Antwortet mit dem berechneten Pfad für den expliziten Kunden mit der angegebenen ID
		