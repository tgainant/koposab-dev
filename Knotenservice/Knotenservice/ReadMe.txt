--- Knotenservice ---

Usage:
	-uri <Base-URI>


Beispiel:
	-uri http://localhost:8084/rest

Erklärung:
	Der Markstatusservice sendet eine Einkaufsliste an den Knotenservice und erhält eine aktualisierte Einkaufsliste mit Knoten.
	Mit der Supermarkt ID wird unterschieden von welchen Markt die Produkte stammen und angefragt werden. Der Knotenservice liefert dann das entsprechende Mapping passend zum Supermarkt.
	
	Supermarkt Ids:
	Güdingen -> 0
	IRL -> 1
	
Requests:
	POST-Request an <Base-URI>/supermarket/<supermarketId>/mapping
	