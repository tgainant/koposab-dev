'use strict';

var utils = require('../utils/writer.js');
var ShoppingLists = require('../controllers/ShoppingListsController');

module.exports.addShoppingList = function addShoppingList (req, res, next) {
  var body = req.swagger.params['body'].value;
  ShoppingLists.addShoppingList(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteShoppingList = function deleteShoppingList (req, res, next) {
  var shoppingListId = req.swagger.params['shoppingListId'].value;
  ShoppingLists.deleteShoppingList(shoppingListId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getShoppingListById = function getShoppingListById (req, res, next) {
  var shoppingListId = req.swagger.params['shoppingListId'].value;
  ShoppingLists.getShoppingListById(shoppingListId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getShoppingLists = function getShoppingLists (req, res, next) {
  ShoppingLists.getShoppingLists()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateShoppingList = function updateShoppingList (req, res, next) {
  var body = req.swagger.params['body'].value;
  ShoppingLists.updateShoppingList(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
