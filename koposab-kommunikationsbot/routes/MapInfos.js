'use strict';

var utils = require('../utils/writer.js');
var MapInfos = require('../controllers/MapInfosController');

module.exports.addMapInfo = function addMapInfo (req, res, next) {
  var body = req.swagger.params['body'].value;
  MapInfos.addMapInfo(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteMapInfo = function deleteMapInfo (req, res, next) {
  var mapId = req.swagger.params['mapId'].value;
  MapInfos.deleteMapInfo(mapId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMapInfoById = function getMapInfoById (req, res, next) {
  var mapId = req.swagger.params['mapId'].value;
  MapInfos.getMapInfoById(mapId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMapInfos = function getMapInfos (req, res, next) {
  MapInfos.getMapInfos()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateMapInfo = function updateMapInfo (req, res, next) {
  var body = req.swagger.params['body'].value;
  MapInfos.updateMapInfo(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
