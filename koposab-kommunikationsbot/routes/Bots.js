'use strict';

var utils = require('../utils/writer.js');
var Bots = require('../controllers/BotsController');

module.exports.addBot = function addBot (req, res, next) {
  var body = req.swagger.params['body'].value;
  Bots.addBot(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteBot = function deleteBot (req, res, next) {
  var botId = req.swagger.params['botId'].value;
  Bots.deleteBot(botId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getBotById = function getBotById (req, res, next) {
  var botId = req.swagger.params['botId'].value;
  Bots.getBotById(botId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getBots = function getBots (req, res, next) {
  Bots.getBots()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateBot = function updateBot (req, res, next) {
  var body = req.swagger.params['body'].value;
  Bots.updateBot(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
