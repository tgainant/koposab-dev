'use strict';

var utils = require('../utils/writer.js');
var Categories = require('../controllers/CategoriesController');

module.exports.addCategory = function addCategory (req, res, next) {
  var body = req.swagger.params['body'].value;
  Categories.addCategory(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteCategory = function deleteCategory (req, res, next) {
  var categoryId = req.swagger.params['categoryId'].value;
  Categories.deleteCategory(categoryId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCategories = function getCategories (req, res, next) {
  Categories.getCategories()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCategoryById = function getCategoryById (req, res, next) {
  var categoryId = req.swagger.params['categoryId'].value;
  Categories.getCategoryById(categoryId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateCategory = function updateCategory (req, res, next) {
  var body = req.swagger.params['body'].value;
  Categories.updateCategory(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
