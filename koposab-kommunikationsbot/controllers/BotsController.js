'use strict';

var Bot = require('../models/Bot').default;

/**
 * Ein Kundenbot hinzufügen
 * Zurückliefern ein Kundenbot
 *
 * body Bot Kundenbot Objekt, das hinzugefügt werden soll
 * returns Bot
 **/
exports.addBot = function(body) {
  return new Promise(function(resolve, reject) {
    let bot = new Bot("", body.name, body.firstName, body.actionsID, body.shoppingList, body.timestampIntro, body.timestampLastAction, body.map, body.customerData);
    if (bot != null) {
      //console.log(bot);
      require('../index').mapper.saveInstance(bot).then((result) => {
        console.log("New Bot:");
        console.log(result);
        //require('../index').mappingInterface.getMapInfo(result.id);
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}


/**
 * Löscht ein Kundenbot
 * 
 *
 * botId String ID des Kundenbot, das wir löschen wollen.
 * no response value expected for this operation
 **/
exports.deleteBot = function(botId) {
  return require('../index').mapper.loadInstance(botId).then((result) => {
    require('../index').mapper.deleteInstance(result).then((queryResult) => {
      if(queryResult.affectedRows == 1){
        return "OK";
      }
      else{
        let error = "No affected rows while deleting instance.";
        require('../index').debugger.error(error);
        return error;
      }
    }).catch((error) => {
      require('../index').debugger.error(error);
      return error;
    });
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Ein Kundenbot-Objekt mit einem id finden
 * Zurückliefern ein Kundenbot
 *
 * botId String ID des Kundenbot, nach dem wir suchen.
 * returns Bot
 **/
exports.getBotById = function(botId) {
  return require('../index').mapper.loadInstance(botId).then((result) => {
    return result.toJSON();
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Alle Kundenbots erhalten
 * Ein Array mit allen Kundenbots erhalten
 *
 * returns List
 **/
exports.getBots = function() {
  return require('../index').mapper.loadInstances(Bot).then((result) => {
    let json = {};
    let instancesAsJSON = [];
    for(let key in result){
      instancesAsJSON.push(result[key].toJSON());
    }
    json.instances = instancesAsJSON;
    return json;
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Aktualisieren eines vorhandenen Kundenbot
 * Zurückliefern ein aktualisiertes Kundenbot
 *
 * body Bot Aktualisiertes Kundenbot Objekt mit dem korrekten id
 * returns Bot
 **/
exports.updateBot = function(body) {
  return new Promise(function(resolve, reject) {
    let bot = new Bot(body.id, body.name, body.firstName, body.actionsID, body.shoppingList, body.timestampIntro, body.timestampLastAction, body.map, body.customerData);
    if (bot != null) {
      require('../index').mapper.saveInstance(bot).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}