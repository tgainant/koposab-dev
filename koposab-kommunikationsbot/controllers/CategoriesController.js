'use strict';

var Category = require('../models/Category').default;

/**
 * Eine Kategorie hinzufügen
 * Zurückliefern eine Kategorie
 *
 * body Category Kategorie Objekt, das hinzugefügt werden soll
 * returns Category
 **/
exports.addCategory = function(body) {
  return new Promise(function(resolve, reject) {
    let category = new Category("", body.description);
    if (category != null) {
      require('../index').mapper.saveInstance(category).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}


/**
 * Löscht eine Kategorie
 * 
 *
 * categoryId String ID der Kategorie, das wir löschen wollen.
 * no response value expected for this operation
 **/
exports.deleteCategory = function(categoryId) {
  return require('../index').mapper.loadInstance(categoryId).then((result) => {
    require('../index').mapper.deleteInstance(result).then((queryResult) => {
      if(queryResult.affectedRows == 1){
        return "OK";
      }
      else{
        let error = "No affected rows while deleting instance.";
        require('../index').debugger.error(error);
        return error;
      }
    }).catch((error) => {
      require('../index').debugger.error(error);
      return error;
    });
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Alle Kategorie erhalten
 * Ein Array mit allen Kategorie erhalten
 *
 * returns List
 **/
exports.getCategories = function() {
  return require('../index').mapper.loadInstances(Category).then((result) => {
    let json = {};
    let instancesAsJSON = [];
    for(let key in result){
      instancesAsJSON.push(result[key].toJSON());
    }
    json.instances = instancesAsJSON;
    return json;
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Eine Kategorie-Objekt mit einem id finden
 * Zurückliefern eine Kategorie
 *
 * categoryId String ID der Kategorie, nach dem wir suchen.
 * returns Category
 **/
exports.getCategoryById = function(categoryId) {
  return require('../index').mapper.loadInstance(categoryId).then((result) => {
    return result.toJSON();
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Aktualisieren einer vorhandenen Kategorie
 * Zurückliefern eine aktualisierte Kategorie
 *
 * body Category Aktualisierte Kategorie Objekt mit dem korrekten id
 * returns Category
 **/
exports.updateCategory = function(body) {
  return new Promise(function(resolve, reject) {
    let category = new Category(body.id, body.description);
    if (category != null) {
      require('../index').mapper.saveInstance(category).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}

