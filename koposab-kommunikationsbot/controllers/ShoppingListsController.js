'use strict';

var ShoppingList = require('../models/ShoppingList').default;

/**
 * Ein Einkaufsliste hinzufügen
 * Zurückliefern ein Einkaufsliste
 *
 * body ShoppingList Einkaufsliste Objekt, das hinzugefügt werden soll
 * returns ShoppingList
 **/
exports.addShoppingList = function(body) {
  return new Promise(function(resolve, reject) {
    let shoppingList = new ShoppingList("", body.products);
    if (shoppingList != null) {
      require('../index').mapper.saveInstance(shoppingList).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}


/**
 * Löscht ein Einkaufsliste
 * 
 *
 * shoppingListId String ID des Einkaufsliste, das wir löschen wollen.
 * no response value expected for this operation
 **/
exports.deleteShoppingList = function(shoppingListId) {
  return require('../index').mapper.loadInstance(shoppingListId).then((result) => {
    require('../index').mapper.deleteInstance(result).then((queryResult) => {
      if(queryResult.affectedRows == 1){
        return "OK";
      }
      else{
        let error = "No affected rows while deleting instance.";
        require('../index').debugger.error(error);
        return error;
      }
    }).catch((error) => {
      require('../index').debugger.error(error);
      return error;
    });
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Ein Einkaufsliste-Objekt mit einem id finden
 * Zurückliefern ein Einkaufsliste
 *
 * shoppingListId String ID des Einkaufsliste, nach dem wir suchen.
 * returns ShoppingList
 **/
exports.getShoppingListById = function(shoppingListId) {
  return require('../index').mapper.loadInstance(shoppingListId).then((result) => {
    return result.toJSON();
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Alle Einkaufslisten erhalten
 * Ein Array mit allen Einkaufslisten erhalten
 *
 * returns List
 **/
exports.getShoppingLists = function() {
  return require('../index').mapper.loadInstances(ShoppingList).then((result) => {
    let json = {};
    let instancesAsJSON = [];
    for(let key in result){
      instancesAsJSON.push(result[key].toJSON());
    }
    json.instances = instancesAsJSON;
    return json;
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Aktualisieren eines vorhandenen Einkaufsliste
 * Zurückliefern ein aktualisiertes Einkaufsliste
 *
 * body ShoppingList Aktualisiertes Einkaufsliste Objekt mit dem korrekten id
 * returns ShoppingList
 **/
exports.updateShoppingList = function(body) {
  return new Promise(function(resolve, reject) {
    let shoppingList = new ShoppingList(body.id, body.products);
    if (shoppingList != null) {
      require('../index').mapper.saveInstance(shoppingList).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}

