'use strict';

var MapInfo = require('../models/MapInfo').default;

/**
 * Ein Map hinzufügen
 * Zurückliefern ein Map
 *
 * body Map Map Objekt, das hinzugefügt werden soll
 * returns Map
 **/
exports.addMapInfo = function(body) {
  return new Promise(function(resolve, reject) {
    let mapInfo = new MapInfo("", body.image);
    if (mapInfo != null) {
      require('../index').mapper.saveInstance(mapInfo).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}


/**
 * Löscht ein Map
 * 
 *
 * mapId String ID des Map, das wir löschen wollen.
 * no response value expected for this operation
 **/
exports.deleteMapInfo = function(mapId) {
  return require('../index').mapper.loadInstance(mapId).then((result) => {
    require('../index').mapper.deleteInstance(result).then((queryResult) => {
      if(queryResult.affectedRows == 1){
        return "OK";
      }
      else{
        let error = "No affected rows while deleting instance.";
        require('../index').debugger.error(error);
        return error;
      }
    }).catch((error) => {
      require('../index').debugger.error(error);
      return error;
    });
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Ein Map-Objekt mit einem id finden
 * Zurückliefern ein Map
 *
 * mapId String ID des Map, nach dem wir suchen.
 * returns Map
 **/
exports.getMapInfoById = function(mapId) {
  return new Promise((resolve, reject) => {
    resolve(require('../index').mappingInterface.mockMapInfo(mapId));
  });
  /*return require('../index').mapper.loadInstance(mapId).then((result) => {
    return result.toJSON();
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });*/
}


/**
 * Alle Maps erhalten
 * Ein Array mit allen Maps erhalten
 *
 * returns List
 **/
exports.getMapInfos = function() {
  return require('../index').mapper.loadInstances(MapInfo).then((result) => {
    let json = {};
    let instancesAsJSON = [];
    for(let key in result){
      instancesAsJSON.push(result[key].toJSON());
    }
    json.instances = instancesAsJSON;
    return json;
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Aktualisieren eines vorhandenen Map
 * Zurückliefern ein aktualisiertes Map
 *
 * body Map Aktualisiertes Map Objekt mit dem korrekten id
 * returns Map
 **/
exports.updateMapInfo = function(body) {
  return new Promise(function(resolve, reject) {
    let mapInfo = new MapInfo(body.id, body.image);
    if (mapInfo != null) {
      require('../index').mapper.saveInstance(mapInfo).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}
