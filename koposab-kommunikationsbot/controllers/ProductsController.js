'use strict';

var Product = require('../models/Product').default;

/**
 * Ein Einkaufsliste-Item hinzufügen
 * Zurückliefern ein Einkaufsliste-Item
 *
 * body Product Einkaufsliste-Item Objekt, das hinzugefügt werden soll
 * returns Product
 **/
exports.addProduct = function(body) {
  return new Promise(function(resolve, reject) {
    let product = new Product("", body.position, body.productName, body.ean, body.quantity, body.bought, body.brand, body.shelf, body.price, body.category);
    if (product != null) {
      require('../index').mapper.saveInstance(product).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}


/**
 * Löscht ein Einkaufsliste-Item
 * 
 *
 * productId String ID des Einkaufsliste-Item, das wir löschen wollen.
 * no response value expected for this operation
 **/
exports.deleteProduct = function(productId) {
  return require('../index').mapper.loadInstance(productId).then((result) => {
    require('../index').mapper.deleteInstance(result).then((queryResult) => {
      if(queryResult.affectedRows == 1){
        return "OK";
      }
      else{
        let error = "No affected rows while deleting instance.";
        require('../index').debugger.error(error);
        return error;
      }
    }).catch((error) => {
      require('../index').debugger.error(error);
      return error;
    });
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Ein Einkaufsliste-Item-Objekt mit einem id finden
 * Zurückliefern ein Einkaufsliste-Item
 *
 * productId String ID des Einkaufsliste-Item, nach dem wir suchen.
 * returns Product
 **/
exports.getProductById = function(productId) {
  return require('../index').mapper.loadInstance(productId).then((result) => {
    return result.toJSON();
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Alle Einkaufsliste-Item erhalten
 * Ein Array mit allen Einkaufsliste-Items erhalten
 *
 * returns List
 **/
exports.getProducts = function() {
  return require('../index').mapper.loadInstances(Product).then((result) => {
    let json = {};
    let instancesAsJSON = [];
    for(let key in result){
      instancesAsJSON.push(result[key].toJSON());
    }
    json.instances = instancesAsJSON;
    return json;
  }).catch((error) => {
    require('../index').debugger.error(error);
    return error;
  });
}


/**
 * Aktualisieren eines vorhandenen Einkaufsliste-Item
 * Zurückliefern ein aktualisiertes Einkaufsliste-Item
 *
 * body Product Aktualisiertes Einkaufsliste-Item Objekt mit dem korrekten id
 * returns Product
 **/
exports.updateProduct = function(body) {
  return new Promise(function(resolve, reject) {
    let product = new Product(body.id, body.position, body.productName, body.ean, body.quantity, body.bought, body.brand, body.shelf, body.price, body.category);
    if (product != null) {
      require('../index').mapper.saveInstance(product).then((result) => {
        resolve(result.toJSON());
      }).catch((error) => {
        require('../index').debugger.error(error);
        reject(error);
      });
    }
    else{
      reject("Could not create instance in memory.");
    }
  });
}

