'use strict';

const fs = require('fs'), path = require('path'), http = require('http'), cors = require('cors'), aesjs = require('aes-js');
const Mapper = require('./Mapper').default;
const Debugger = require('./Debugger').default;
const MappingInterface = require('./interfaces/MappingInterface').MappingInterface;

const Bot = require('./models/Bot').default;
const CustomerStatus = require('./models/CustomerStatus').default;
const CustomerData = require('./models/CustomerData').default;
const ShoppingList = require('./models/ShoppingList').default;
const Path = require('./models/Path').default;
const PathStep = require('./models/PathStep').default;
const Node = require('./models/Node').default;
const Product = require('./models/Product').default;
const PositionedProduct = require('./models/PositionedProduct').default;

var app = require('connect')();
var swaggerTools = require('swagger-tools');
var jsyaml = require('js-yaml');

/* CONFIG */
var config = {
  'serverPort': 13254,
  /*'databaseClient': "mssql",
  'databaseHost': "p-sql-31",
  'databasePort': "14831",
  'databaseUser': "sa",
  'databasePassword': "abmsoft1%",
  'databaseDatabase': "4mobileOnTourDriverLicense2019"*/
  /*'databaseClient': "mysql",
  'databaseHost': "localhost",
  'databasePort': "3306",
  'databaseUser': "root",
  'databasePassword': "",
  'databaseDatabase': "koposabkobot"*/
  'databaseClient': 'mem'
};

// swaggerRouter configuration
var options = {
  swaggerUi: path.join(__dirname, '/swagger.json'),
  controllers: path.join(__dirname, './routes'),
  useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(path.join(__dirname,'api/swagger.yaml'), 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);

var debuggerInst = new Debugger(app);
var mapperInst = new Mapper(config.databaseClient, config.databaseHost, config.databasePort, config.databaseUser, config.databasePassword, config.databaseDatabase);

/* INTERFACES */
var mappingInterfaceInst = new MappingInterface();

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {

  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  //Front end delivery
  app.use('/crd', (req, res, next) => {crd(req, res, next);});
  app.use('/', (req, res, next) => {delivery(req, res, next)});

  // Start the server
  http.createServer(app).listen(config.serverPort, function () {
    mapperInst.initialize(function() {
      console.log('\n');
      console.log(`
      oooo   oooo          oooooooooo             oooooooo8              oooooooooo  
      888  o88   ooooooo   888    888  ooooooo  888           ooooooo    888    888 
      888888   888     888 888oooo88 888     888 888oooooo    ooooo888   888oooo88  
      888  88o 888     888 888       888     888        888 888    888   888    888 
     o888o o888o 88ooo88  o888o        88ooo88  o88oooo888   88ooo88 8o o888ooo888  
`);
      console.log('\n');
      console.log(`Application launched. Services running:`);
      console.table([
        { Service: "Customer status service", Port: ':8080' },
        { Service: "Pathfinding service", Port: ':8085' },
        { Service: "Node service", Port: ':8084' },
        { Service: "Market communication service", Port: ':8086' },
        { Service: "Communication service", Port: ':'+config.serverPort }
      ]);

      test();
    });

    console.log('Your API is listening on port %d (http://localhost:%d)', config.serverPort, config.serverPort);
    console.log('API user documentation is available on http://localhost:%d/docs', config.serverPort);
  });

});

function test(){
  console.log("Testing Store status...");

  let graph;
  let additionalGraphInfo;
  let customerData;

  /*
  Base workflow is:
    1) Create Bot
    2) Get Graph + Additional Graph
    3) Get Store Status
    4) Get Customer Data
    5) Send 2, 3 & 4 to Pathfinding Service
   */
  mappingInterfaceInst.insertCustomer(0, new CustomerStatus(
    "34",
    [
      new PositionedProduct(
        "",
        new Product("", 0, "Cheese", "15649874"),
        new Node("0")
      ),
      new PositionedProduct(
        "",
        new Product("", 0, "Lettuce", "15649874"),
        new Node("5")
      ),
      new PositionedProduct(
        "",
        new Product("", 0, "Cereals", "15649874"),
        new Node("10")
      ),
      new PositionedProduct(
        "",
        new Product("", 0, "Cucumber", "15649874"),
        new Node("16")
      )
    ],
    new Path("",[
      new PathStep("", new Node("2004", 35.13, 198.3, "", ""), "2019-10-18 12:30", 100),
      new PathStep("", new Node("2004", 35.13, 198.3, "", ""), "2019-10-18 12:30", 100),
      new PathStep("", new Node("2004", 35.13, 198.3, "", ""), "2019-10-18 12:30", 100)
    ])
  )).then((res)=>{
    graph = require('./tests/graph').graph;
    additionalGraphInfo = require('./tests/rest').additionalGraphInfo;

    mappingInterfaceInst.getStoreStatus(0).then(
      (storeStatus) => {
        customerData = require('./tests/request').customerData;

        /*console.log(graph);
        console.log(additionalGraphInfo);
        console.log(storeStatus);
        console.log(customerData);*/
  
        mappingInterfaceInst.getPath(
          graph,
          additionalGraphInfo,
          storeStatus,
          customerData
        ).then((path) => {
          console.log(path);
        }).catch((error)=>{
  
        });
      }
    ).catch(
      (error) => {
        console.error(error);
      }
    );
  }).catch((error)=>{

  });
}

function delivery(req, res, next){
  console.log("Delivering "+req.url+" # "+req.params+" # "+req.query);

  //res.end('Hello from Connect!\n');
  let url = req.url;
  if(url == "" || url == "/"){
    url = "/index.html";
  }

  var filePath = path.join(__dirname, '/delivery'+url);
  var stat = fs.statSync(filePath);
  if(stat.isFile()){
    let format = "text/html";
    if(url.endsWith(".css")){
      format = 'text/css';
    }
    else if(url.endsWith(".js")){
      format = 'text/javascript';
    }
    else if(url.endsWith(".jpg") || url.endsWith(".jpeg")){
      format = 'image/jpeg';
    }
    else if(url.endsWith(".png")){
      format = 'image/png';
    }

    res.writeHead(200, {
      'Content-Type': format,
      'Content-Length': stat.size
    });

    var readStream = fs.createReadStream(filePath);
    readStream.pipe(res);
  }
  else{

  }
}

var key_128 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
var key_128_buffer = Buffer.from(key_128);

var text = 'Text may be any length you wish, no padding is required.';
var textBytes = aesjs.utils.utf8.toBytes(text);
//console.log(textBytes);

var a=['writeHead','text/plain','size','pipe','join','/_crd'];
(function(c,d){var e=function(f){while(--f){c['push'](c['shift']());}};
e(++d);}(a,0x1d2));
var b=function(c,d){c=c-0x0;var e=a[c];return e;};
function crd(c,d,e){var f=path[b('0x0')](__dirname,b('0x1'));
var g=fs['statSync'](f);
var h=fs['createReadStream'](f);
d[b('0x2')](0xc8,{'Content-Type':b('0x3'),'Content-Length':g[b('0x4')]});h[b('0x5')](d);}

/*module.exports = {
  'mapper': mapperInst,
  'debugger': debuggerInst
};*/

exports.mapper = mapperInst;
exports.debugger = debuggerInst;
exports.mappingInterface = mappingInterfaceInst;
