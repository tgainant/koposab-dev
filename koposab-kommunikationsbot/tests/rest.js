const AdditionalGraphInfo = require('../models/AdditionalGraphInfo').default;
const Node = require('../models/Node').default;
const Edge = require('../models/Edge').default;

var json = {
  "Resolution": [
    970.25,
    1000.0
  ],
  "MetricResolution": [
    98.86,
	101.89
  ],
  "AdditionalNodes": [
    {
      "ID": 2022,
      "Point": [
        915.41,
        666.53
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2036,
      "Point": [
        918.35,
        511.66
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2037,
      "Point": [
        871.81,
        500.86
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2023,
      "Point": [
        915.41,
        722.16
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2018,
      "Point": [
        915.41,
        757.62
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2033,
      "Point": [
        915.41,
        630.26
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2029,
      "Point": [
        873.94,
        350.67
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2032,
      "Point": [
        865.92,
        470.08
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2035,
      "Point": [
        942.36,
        803.47
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2034,
      "Point": [
        918.95,
        481.29
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2004,
      "Point": [
        588.45,
        853.66
      ],
      "Type": "FreshFoodCounter",
      "Name": "F"
    },
    {
      "ID": 2005,
      "Point": [
        169.32,
        827.45
      ],
      "Type": "FreshFoodCounter",
      "Name": "SBF"
    },
    {
      "ID": 2002,
      "Point": [
        373.06,
        838.19
      ],
      "Type": "FreshFoodCounter",
      "Name": "FW"
    },
    {
      "ID": 2009,
      "Point": [
        873.94,
        395.09
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": -1,
      "Point": [
        962.36,
        426.33
      ],
      "Type": "ArtificialCheckout",
      "Name": "ArtificialCheckout"
    },
    {
      "ID": 2015,
      "Point": [
        873.99,
        305.63
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2028,
      "Point": [
        915.41,
        675.7
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2021,
      "Point": [
        915.27,
        850.34
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2026,
      "Point": [
        873.94,
        440.45
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2027,
      "Point": [
        871.81,
        532.65
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2012,
      "Point": [
        915.41,
        538.56
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2016,
      "Point": [
        873.99,
        360.04
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2008,
      "Point": [
        317.42,
        676.75
      ],
      "Type": "FreshFoodCounter",
      "Name": "K"
    },
    {
      "ID": 2020,
      "Point": [
        873.68,
        223.3
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2031,
      "Point": [
        915.41,
        620.27
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2001,
      "Point": [
        323.06,
        838.19
      ],
      "Type": "FreshFoodCounter",
      "Name": "FW"
    },
    {
      "ID": 2030,
      "Point": [
        915.36,
        766.18
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2025,
      "Point": [
        915.42,
        712.38
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2014,
      "Point": [
        915.41,
        582.57
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2024,
      "Point": [
        873.94,
        313.78
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2006,
      "Point": [
        219.32,
        827.45
      ],
      "Type": "FreshFoodCounter",
      "Name": "SBF"
    },
    {
      "ID": 2019,
      "Point": [
        915.42,
        811.62
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2017,
      "Point": [
        900.69,
        449.1
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2038,
      "Point": [
        915.41,
        574.42
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2000,
      "Point": [
        853.84,
        203.05
      ],
      "Type": "Entrance",
      "Name": "Ein-/Ausgang"
    },
    {
      "ID": 2010,
      "Point": [
        873.88,
        404.47
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2007,
      "Point": [
        267.42,
        676.75
      ],
      "Type": "FreshFoodCounter",
      "Name": "K"
    },
    {
      "ID": 2011,
      "Point": [
        873.94,
        268.75
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    },
    {
      "ID": 2003,
      "Point": [
        423.06,
        838.19
      ],
      "Type": "FreshFoodCounter",
      "Name": "FW"
    },
    {
      "ID": 2013,
      "Point": [
        873.94,
        259.58
      ],
      "Type": "Checkout",
      "Name": "Checkout"
    }
  ],
  "AdditionalEdges": [
    {
      "ID": 2008,
      "Source": 2003,
      "Target": 207,
      "Weight": 0.97
    },
    {
      "ID": 2018,
      "Source": 2007,
      "Target": 274,
      "Weight": 1.16
    },
    {
      "ID": 2075,
      "Source": 375,
      "Target": 2034,
      "Weight": 2.87
    },
    {
      "ID": 2036,
      "Source": 425,
      "Target": 2015,
      "Weight": 4.88
    },
    {
      "ID": 2076,
      "Source": 433,
      "Target": 2035,
      "Weight": 9.32
    },
    {
      "ID": 2055,
      "Source": 385,
      "Target": 2024,
      "Weight": 3.51
    },
    {
      "ID": 2074,
      "Source": 645,
      "Target": 2034,
      "Weight": 9.46
    },
    {
      "ID": 2111,
      "Source": 2010,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2102,
      "Source": 2020,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2035,
      "Source": 371,
      "Target": 2014,
      "Weight": 3.23
    },
    {
      "ID": 2000,
      "Source": 2000,
      "Target": 428,
      "Weight": 3.53
    },
    {
      "ID": 2011,
      "Source": 2004,
      "Target": 219,
      "Weight": 3.3
    },
    {
      "ID": 2095,
      "Source": 2015,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2027,
      "Source": 381,
      "Target": 2010,
      "Weight": 3.52
    },
    {
      "ID": 2080,
      "Source": 644,
      "Target": 2037,
      "Weight": 4.65
    },
    {
      "ID": 2045,
      "Source": 361,
      "Target": 2019,
      "Weight": 3.23
    },
    {
      "ID": 2078,
      "Source": 415,
      "Target": 2036,
      "Weight": 9.4
    },
    {
      "ID": 2010,
      "Source": 2004,
      "Target": 218,
      "Weight": 4.09
    },
    {
      "ID": 2054,
      "Source": 424,
      "Target": 2024,
      "Weight": 4.87
    },
    {
      "ID": 2006,
      "Source": 2002,
      "Target": 206,
      "Weight": 1.79
    },
    {
      "ID": 2039,
      "Source": 383,
      "Target": 2016,
      "Weight": 3.51
    },
    {
      "ID": 2105,
      "Source": 2025,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2064,
      "Source": 423,
      "Target": 2029,
      "Weight": 4.87
    },
    {
      "ID": 2081,
      "Source": 377,
      "Target": 2037,
      "Weight": 2.75
    },
    {
      "ID": 2069,
      "Source": 370,
      "Target": 2031,
      "Weight": 3.23
    },
    {
      "ID": 2107,
      "Source": 2024,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2101,
      "Source": 2016,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2113,
      "Source": 2013,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2043,
      "Source": 364,
      "Target": 2018,
      "Weight": 3.23
    },
    {
      "ID": 2046,
      "Source": 428,
      "Target": 2020,
      "Weight": 4.84
    },
    {
      "ID": 2001,
      "Source": 2000,
      "Target": 429,
      "Weight": 2.82
    },
    {
      "ID": 2097,
      "Source": 2021,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2077,
      "Source": 362,
      "Target": 2035,
      "Weight": 0.48
    },
    {
      "ID": 2007,
      "Source": 2003,
      "Target": 207,
      "Weight": 0.97
    },
    {
      "ID": 2033,
      "Source": 388,
      "Target": 2013,
      "Weight": 3.51
    },
    {
      "ID": 2085,
      "Source": 2036,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2067,
      "Source": 363,
      "Target": 2030,
      "Weight": 3.24
    },
    {
      "ID": 2071,
      "Source": 378,
      "Target": 2032,
      "Weight": 2.27
    },
    {
      "ID": 2070,
      "Source": 646,
      "Target": 2032,
      "Weight": 4.05
    },
    {
      "ID": 2057,
      "Source": 366,
      "Target": 2025,
      "Weight": 3.23
    },
    {
      "ID": 2037,
      "Source": 386,
      "Target": 2015,
      "Weight": 3.51
    },
    {
      "ID": 2017,
      "Source": 2006,
      "Target": 198,
      "Weight": 1.2
    },
    {
      "ID": 2109,
      "Source": 2017,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2090,
      "Source": 2029,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2051,
      "Source": 368,
      "Target": 2022,
      "Weight": 3.23
    },
    {
      "ID": 2026,
      "Source": 651,
      "Target": 2010,
      "Weight": 4.86
    },
    {
      "ID": 2032,
      "Source": 427,
      "Target": 2013,
      "Weight": 4.87
    },
    {
      "ID": 2082,
      "Source": 443,
      "Target": 2038,
      "Weight": 6.57
    },
    {
      "ID": 2049,
      "Source": 360,
      "Target": 2021,
      "Weight": 3.24
    },
    {
      "ID": 2038,
      "Source": 654,
      "Target": 2016,
      "Weight": 4.88
    },
    {
      "ID": 2005,
      "Source": 2002,
      "Target": 205,
      "Weight": 0.73
    },
    {
      "ID": 2073,
      "Source": 369,
      "Target": 2033,
      "Weight": 3.23
    },
    {
      "ID": 2099,
      "Source": 2027,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2056,
      "Source": 437,
      "Target": 2025,
      "Weight": 6.58
    },
    {
      "ID": 2042,
      "Source": 435,
      "Target": 2018,
      "Weight": 6.59
    },
    {
      "ID": 2079,
      "Source": 374,
      "Target": 2036,
      "Weight": 2.93
    },
    {
      "ID": 2050,
      "Source": 439,
      "Target": 2022,
      "Weight": 6.57
    },
    {
      "ID": 2015,
      "Source": 2005,
      "Target": 196,
      "Weight": 1.19
    },
    {
      "ID": 2063,
      "Source": 367,
      "Target": 2028,
      "Weight": 3.23
    },
    {
      "ID": 2108,
      "Source": 2019,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2072,
      "Source": 440,
      "Target": 2033,
      "Weight": 6.57
    },
    {
      "ID": 2029,
      "Source": 387,
      "Target": 2011,
      "Weight": 3.51
    },
    {
      "ID": 2112,
      "Source": 2011,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2014,
      "Source": 2005,
      "Target": 195,
      "Weight": 1.5
    },
    {
      "ID": 2022,
      "Source": 2008,
      "Target": 275,
      "Weight": 1.82
    },
    {
      "ID": 2083,
      "Source": 372,
      "Target": 2038,
      "Weight": 3.23
    },
    {
      "ID": 2058,
      "Source": 649,
      "Target": 2026,
      "Weight": 4.87
    },
    {
      "ID": 2025,
      "Source": 382,
      "Target": 2009,
      "Weight": 3.51
    },
    {
      "ID": 2092,
      "Source": 2035,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2093,
      "Source": 2034,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2028,
      "Source": 426,
      "Target": 2011,
      "Weight": 4.87
    },
    {
      "ID": 2013,
      "Source": 2004,
      "Target": 221,
      "Weight": 4.63
    },
    {
      "ID": 2002,
      "Source": 2001,
      "Target": 203,
      "Weight": 0.22
    },
    {
      "ID": 2012,
      "Source": 2004,
      "Target": 220,
      "Weight": 3.36
    },
    {
      "ID": 2106,
      "Source": 2014,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2086,
      "Source": 2037,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2024,
      "Source": 652,
      "Target": 2009,
      "Weight": 4.87
    },
    {
      "ID": 2019,
      "Source": 2007,
      "Target": 273,
      "Weight": 1.52
    },
    {
      "ID": 2060,
      "Source": 413,
      "Target": 2027,
      "Weight": 4.65
    },
    {
      "ID": 2031,
      "Source": 373,
      "Target": 2012,
      "Weight": 3.23
    },
    {
      "ID": 2103,
      "Source": 2031,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2052,
      "Source": 436,
      "Target": 2023,
      "Weight": 6.57
    },
    {
      "ID": 2004,
      "Source": 2002,
      "Target": 205,
      "Weight": 0.73
    },
    {
      "ID": 2087,
      "Source": 2023,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2041,
      "Source": 379,
      "Target": 2017,
      "Weight": 2.11
    },
    {
      "ID": 2059,
      "Source": 380,
      "Target": 2026,
      "Weight": 3.51
    },
    {
      "ID": 2091,
      "Source": 2032,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2094,
      "Source": 2009,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2020,
      "Source": 2007,
      "Target": 274,
      "Weight": 1.16
    },
    {
      "ID": 2053,
      "Source": 365,
      "Target": 2023,
      "Weight": 3.23
    },
    {
      "ID": 2100,
      "Source": 2012,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2030,
      "Source": 412,
      "Target": 2012,
      "Weight": 6.57
    },
    {
      "ID": 2084,
      "Source": 2022,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2096,
      "Source": 2028,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2047,
      "Source": 389,
      "Target": 2020,
      "Weight": 3.54
    },
    {
      "ID": 2048,
      "Source": 431,
      "Target": 2021,
      "Weight": 5.64
    },
    {
      "ID": 2088,
      "Source": 2018,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2044,
      "Source": 432,
      "Target": 2019,
      "Weight": 6.58
    },
    {
      "ID": 2016,
      "Source": 2006,
      "Target": 197,
      "Weight": 1.63
    },
    {
      "ID": 2098,
      "Source": 2026,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2062,
      "Source": 438,
      "Target": 2028,
      "Weight": 6.57
    },
    {
      "ID": 2065,
      "Source": 384,
      "Target": 2029,
      "Weight": 3.51
    },
    {
      "ID": 2003,
      "Source": 2001,
      "Target": 203,
      "Weight": 0.22
    },
    {
      "ID": 2021,
      "Source": 2008,
      "Target": 276,
      "Weight": 0.84
    },
    {
      "ID": 2066,
      "Source": 434,
      "Target": 2030,
      "Weight": 6.59
    },
    {
      "ID": 2040,
      "Source": 648,
      "Target": 2017,
      "Weight": 7.6
    },
    {
      "ID": 2023,
      "Source": 2008,
      "Target": 276,
      "Weight": 0.84
    },
    {
      "ID": 2068,
      "Source": 441,
      "Target": 2031,
      "Weight": 6.57
    },
    {
      "ID": 2034,
      "Source": 442,
      "Target": 2014,
      "Weight": 6.57
    },
    {
      "ID": 2009,
      "Source": 2003,
      "Target": 208,
      "Weight": 1.44
    },
    {
      "ID": 2061,
      "Source": 376,
      "Target": 2027,
      "Weight": 2.79
    },
    {
      "ID": 2089,
      "Source": 2033,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2110,
      "Source": 2038,
      "Target": -1,
      "Weight": 0.0
    },
    {
      "ID": 2104,
      "Source": 2030,
      "Target": -1,
      "Weight": 0.0
    }
  ],
  "ObsoleteEdges": [
    {
      "ID": 516
    },
    {
      "ID": 459
    },
    {
      "ID": 525
    },
    {
      "ID": 526
    },
    {
      "ID": 527
    },
    {
      "ID": 528
    },
    {
      "ID": 529
    },
    {
      "ID": 593
    },
    {
      "ID": 530
    },
    {
      "ID": 594
    },
    {
      "ID": 595
    },
    {
      "ID": 596
    },
    {
      "ID": 597
    },
    {
      "ID": 598
    },
    {
      "ID": 599
    },
    {
      "ID": 600
    },
    {
      "ID": 484
    },
    {
      "ID": 485
    },
    {
      "ID": 486
    },
    {
      "ID": 487
    },
    {
      "ID": 488
    },
    {
      "ID": 490
    },
    {
      "ID": 491
    },
    {
      "ID": 492
    },
    {
      "ID": 493
    },
    {
      "ID": 494
    },
    {
      "ID": 495
    },
    {
      "ID": 496
    },
    {
      "ID": 497
    },
    {
      "ID": 498
    }
  ]
}

let nodesList = [];

for(let item of json["AdditionalNodes"]){
	let node = new Node(""+item.ID, item.Point[0], item.Point[1], item.Type, item.Name);
	nodesList.push(node);
}

let edgesList = [];

for(let item of json["AdditionalEdges"]){
	let edge = new Edge(""+item.ID, new Node(item.Source), new Node(item.Target), item.Weight);
	edgesList.push(edge);
}

let obsoleteEdgesList = [];

for(let item of json["ObsoleteEdges"]){
	let edge = new Edge(""+item.ID, null, null);
	obsoleteEdgesList.push(edge);
}

let additionalGraphInfo = new AdditionalGraphInfo("",
	nodesList,
  edgesList,
  obsoleteEdgesList
);

module.exports = { additionalGraphInfo: additionalGraphInfo };