const CustomerData = require('../models/CustomerData').default;
const PositionedProduct = require('../models/PositionedProduct').default;
const Product = require('../models/Product').default;
const Node = require('../models/Node').default;

var json = {
  "shoppingList": [
    {
      "item":"Milk",
      "ean":"15649874",
      "node":"1343"
    },
    {
      "item": "Cheese (K)",
      "ean":"15649874",
      "node": "-999"
    },
    {
      "item": "Lettuce",
      "ean":"15649874",
      "node": "159"
    },
    {
      "item": "Cereals",
      "ean":"15649874",
      "node": "1"
    },
    {
      "item": "Cereals",
      "ean":"15649874",
      "node": "401"
    },
    {
      "item": "Meat (F)",
      "ean":"15649874",
      "node": "-999"
    },
    {
      "item": "Cereals",
      "ean":"15649874",
      "node": "1423"
    },
    {
      "item": "Cereals",
      "ean":"15649874",
      "node": "10"
    },
    {
      "item": "Tomatoes",
      "ean":"15649874",
      "node": "545"
    }
  ],
  "age":"23",
  "ageAccuracy":"exact",
  "gender":"m"
}

let customerShoppingList = [];
for(let index in json["shoppingList"]){
    let item = json["shoppingList"][index];
    customerShoppingList.push(
        new PositionedProduct(
            "",
            new Product("", index, item.item, item.ean),
            new Node(item.node)
        )
    );
}

var customerData = new CustomerData("",
    customerShoppingList,
    json["age"],
    json["ageAccuracy"],
    json["gender"]
);

module.exports = { customerData: customerData };