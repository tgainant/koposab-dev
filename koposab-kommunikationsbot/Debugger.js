"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require('fs');
const crashFileName = "crashes";
const logFileName = "logs";
class Debugger {
    constructor(origin) {
        this.origin = '';
        this.active = true;
        this.origin = origin.constructor.name;
        process.on("uncaughtException", (err, origin) => {
            /*fs.writeSync(
              process.stderr.fd,
              `Caught exception: ${err}\n` +
              `Exception origin: ${origin}`
            );*/
            this.crash(err);
        });
    }
    crash(content) {
        console.log(content);
        fs.writeFile(crashFileName, content + "\n", function (err) {
            if (err) {
                return console.log(err);
            }
        });
        //TODO email se@abmsoft.de
    }
    info(content) {
        console.log(content);
        this.log(content);
    }
    error(content) {
        console.error(content);
        this.log(content);
    }
    warning(content) {
        console.log(content);
        this.log(content);
    }
    log(content) {
        fs.appendFile(logFileName, content + "\n", function (err) {
            if (err) {
                return console.log(err);
            }
        });
    }
}
exports.default = Debugger;
