const fs = require('fs');

const crashFileName = "crashes";
const logFileName = "logs";

class Debugger{
    private origin:string = '';
    public active:boolean = true;

    constructor(origin:any){
        this.origin = origin.constructor.name;

        (process as NodeJS.EventEmitter).on("uncaughtException", (err, origin) => {
            /*fs.writeSync(
              process.stderr.fd,
              `Caught exception: ${err}\n` +
              `Exception origin: ${origin}`
            );*/
            this.crash(err);
        });
    }

    crash(content:string){
        console.log(content);
        
        fs.writeFile(crashFileName, content+"\n", function(err:any) {
            if(err) {
                return console.log(err);
            }
        });

        //TODO email se@abmsoft.de
    }

    info(content:string){
        console.log(content);
        this.log(content);
    }

    error(content:string){
        console.error(content);
        this.log(content);
    }

    warning(content:string){
        console.log(content);
        this.log(content);
    }

    log(content:string){
        fs.appendFile(logFileName, content+"\n", function(err:any) {
            if(err) {
                return console.log(err);
            }
        });
    }
}

export default Debugger;