"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const MapInfo_1 = __importDefault(require("../models/MapInfo"));
const StoreStatus_1 = __importDefault(require("../models/StoreStatus"));
const Node_1 = __importDefault(require("../models/Node"));
const Interface_1 = require("./Interface");
const PositionedProduct_1 = __importDefault(require("../models/PositionedProduct"));
const Path_1 = __importDefault(require("../models/Path"));
const PathStep_1 = __importDefault(require("../models/PathStep"));
const CustomerStatus_1 = __importDefault(require("../models/CustomerStatus"));
const Product_1 = __importDefault(require("../models/Product"));
const Category_1 = __importDefault(require("../models/Category"));
const fetch = require('node-fetch');
const FormData = require('form-data');
const fs = require('fs');
const path = require("path");
class MappingInterface extends Interface_1.Interface {
    constructor() {
        super();
        this.geofencing_ServiceUrl = "";
        this.positionedProduct_ServiceUrl = "https://koposab-nodeservice.azurewebsites.net/nodeservice/nodes_for_shoppinglist";
        this.customerStatus_ServiceUrl = "http://localhost:8080/rest";
        this.path_ServiceUrl = "http://localhost:8085/rest/mapf/coop/pathfinding";
        this.mapInfo_ServiceUrl = "";
        this.crypting_ServiceUrl = "";
        this.market_ServiceUrl = "";
    }
    thread() {
        super.thread();
    }
    insertCustomer(supermarketId, customerStatus) {
        return new Promise((resolve, reject) => {
            let productList = [];
            for (let positionedProduct of customerStatus.shoppingList) {
                productList.push({
                    "item": positionedProduct.product.productName,
                    "ean": positionedProduct.product.ean,
                    "node": positionedProduct.node.id
                });
            }
            let steps = [];
            for (let pathStep of customerStatus.estimatedPath.nodes) {
                let eta = new Date(pathStep.eta);
                steps.push({
                    "node": {
                        "ID": pathStep.node.id,
                        "Point": [pathStep.node.x, pathStep.node.y]
                    },
                    "eta": {
                        "date": {
                            "year": eta.getFullYear(),
                            "month": eta.getMonth(),
                            "day": eta.getDate()
                        },
                        "time": {
                            "hour": eta.getHours(),
                            "minute": eta.getMinutes(),
                            "second": eta.getSeconds(),
                            "nano": eta.getMilliseconds()
                        }
                    },
                    "duration": {
                        "seconds": pathStep.duration,
                        "nanos": 0
                    }
                });
            }
            let body = {
                "shoppingList": productList,
                "estimatedPath": steps
            };
            let url = this.customerStatus_ServiceUrl + "/supermarket/" + supermarketId + "/newcustomer/" + customerStatus.id;
            fetch(url, { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(body) }).then((response) => {
                response.text();
            }).then((res) => {
                resolve(res);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getStoreStatus(supermarketId) {
        console.log(supermarketId);
        return new Promise((resolve, reject) => {
            fetch(this.customerStatus_ServiceUrl + '/supermarket/' + supermarketId + '/status').then((resRaw) => resRaw.text()).then((response) => {
                let res = JSON.parse(response);
                let result = new StoreStatus_1.default();
                //@ts-ignore
                for (let id in res['customers']) {
                    let shoppingList = [];
                    //@ts-ignore
                    for (let productRaw of res['customers'][id]['shoppingList']) {
                        let newProduct = new Product_1.default("", 0, productRaw['item'], productRaw['ean'], 0, false, "", 0, 0, new Category_1.default());
                        shoppingList.push(new PositionedProduct_1.default("", newProduct, new Node_1.default(productRaw['node'])));
                    }
                    let estimatedPath = new Path_1.default("");
                    //@ts-ignore
                    for (let obj of res['customers'][id]['estimatedPath']) {
                        estimatedPath.nodes.push(new PathStep_1.default("", 
                        //@ts-ignore
                        new Node_1.default(obj["node"]["ID"], obj["node"]["Point"][0], obj["node"]["Point"][1]), 
                        //@ts-ignore
                        new Date(obj["eta"]["date"]["year"], obj["eta"]["date"]["month"], obj["eta"]["date"]["day"], obj["eta"]["time"]["hour"], obj["eta"]["time"]["minute"], obj["eta"]["time"]["second"], obj["eta"]["time"]["nano"]), 
                        //@ts-ignore
                        parseFloat(obj["duration"]["seconds"])));
                    }
                    let newCustomerStatus = new CustomerStatus_1.default(id, shoppingList, estimatedPath);
                    result.customers.push(newCustomerStatus);
                }
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        });
    }
    getPath(graph, additionalGraphInfo, storeStatus, customerData) {
        let nodesList = [];
        let edgesList = [];
        let additionalNodesList = [];
        let additionalEdgesList = [];
        let obsoleteEdgesList = [];
        let customersList = {};
        let customerDataObj = {};
        for (let node of graph.nodes) {
            let nodeAsJSON = {
                "ID": (parseInt(node.id) || 0),
                "Point": [node.x, node.y]
            };
            nodesList.push(nodeAsJSON);
        }
        for (let edge of graph.edges) {
            let edgeAsJSON = {
                "ID": (parseInt(edge.id) || 0),
                "Source": (parseInt(edge.source.id) || 0),
                "Target": (parseInt(edge.target.id) || 0),
                "Weight": edge.weight
            };
            edgesList.push(edgeAsJSON);
        }
        for (let node of additionalGraphInfo.additionalNodes) {
            let nodeAsJSON = {
                "ID": (parseInt(node.id) || 0),
                "Point": [node.x, node.y],
                "Type": node.type,
                "Name": node.name
            };
            additionalNodesList.push(nodeAsJSON);
        }
        for (let edge of additionalGraphInfo.additionalEdges) {
            let edgeAsJSON = {
                "ID": (parseInt(edge.id) || 0),
                "Source": (parseInt(edge.source.id) || 0),
                "Target": (parseInt(edge.target.id) || 0),
                "Weight": edge.weight
            };
            additionalEdgesList.push(edgeAsJSON);
        }
        for (let edge of additionalGraphInfo.obsoleteEdges) {
            let edgeAsJSON = {
                "ID": (parseInt(edge.id) || 0)
            };
            obsoleteEdgesList.push(edgeAsJSON);
        }
        for (let customer of storeStatus.customers) {
            let shoppingList = [];
            let estimatedPath = [];
            for (let positionedProduct of customer.shoppingList) {
                shoppingList.push({
                    "item": positionedProduct.product.productName,
                    "ean": positionedProduct.product.ean,
                    "node": (parseInt(positionedProduct.node.id) || 0)
                });
            }
            for (let pathStep of customer.estimatedPath.nodes) {
                let eta = new Date(pathStep.eta);
                estimatedPath.push({
                    "node": {
                        "ID": (parseInt(pathStep.node.id) || 0),
                        "Point": [pathStep.node.x, pathStep.node.y]
                    },
                    "eta": {
                        "date": {
                            "year": eta.getFullYear(),
                            "month": eta.getMonth(),
                            "day": eta.getDate()
                        },
                        "time": {
                            "hour": eta.getHours(),
                            "minute": eta.getMinutes(),
                            "second": eta.getSeconds(),
                            "nano": eta.getMilliseconds()
                        }
                    },
                    "duration": {
                        "seconds": pathStep.duration,
                        "nanos": 0
                    }
                });
            }
            let customerAsJSON = {
                "shoppingList": shoppingList,
                "estimatedPath": estimatedPath
            };
            //@ts-ignore
            customersList[customer.id] = customerAsJSON;
        }
        let customerShoppingList = [];
        for (let positionedProduct of customerData.shoppingList) {
            customerShoppingList.push({
                "item": positionedProduct.product.productName,
                "ean": positionedProduct.product.ean,
                "node": (parseInt(positionedProduct.node.id) || 0)
            });
        }
        customerDataObj = {
            "shoppingList": customerShoppingList,
            "age": customerData.age,
            "ageAccuracy": customerData.ageAccuracy,
            "gender": customerData.gender
        };
        let json = {
            "Graph": {
                "Resolution": [graph.xResolution, graph.yResolution],
                "Nodes": nodesList,
                "Edges": edgesList
            },
            "AdditionalGraphInfo": {
                "AdditionalNodes": additionalNodesList,
                "AdditionalEdges": additionalEdgesList,
                "ObsoleteEdges": obsoleteEdgesList
            },
            "StoreStatus": {
                "customers": customersList
            },
            "Customer": customerDataObj
        };
        return new Promise((resolve, reject) => {
            fetch(this.path_ServiceUrl, { method: 'POST', body: JSON.stringify(json) }).then((resRaw) => resRaw.text()).then((resStr) => {
                let res = JSON.parse(resStr);
                let result = new Path_1.default();
                //@ts-ignore
                for (let obj of res["estimatedPath"]) {
                    result.nodes.push(new PathStep_1.default("", 
                    //@ts-ignore
                    new Node_1.default(obj["node"]["ID"], obj["node"]["Point"][0], obj["node"]["Point"][1]), 
                    //@ts-ignore
                    new Date(obj["eta"]["date"]["year"], obj["eta"]["date"]["month"], obj["eta"]["date"]["day"], obj["eta"]["time"]["hour"], obj["eta"]["time"]["minute"], obj["eta"]["time"]["second"], obj["eta"]["time"]["nano"]), 
                    //@ts-ignore
                    parseFloat(obj["duration"]["seconds"])));
                }
                resolve(result);
            });
        });
    }
    /* ======================================= */
    getPositionedProductListFromShoppingList(shopppingList) {
        //console.log(shopppingList.toJSON());
        return new Promise((resolve, reject) => {
            let body = {
                "shoppingList": shopppingList.toJSON(true)
            };
            fetch(this.positionedProduct_ServiceUrl, { method: 'POST', body: JSON.stringify(body) }).then((response) => response.text()).then(((rawJson) => {
                let rawResult = JSON.parse(rawJson);
                let result = [];
                // @ts-ignore
                for (let raw of rawResult.positionedProducts) {
                    let newPositionedProduct = new PositionedProduct_1.default();
                    newPositionedProduct.fromJSON(raw);
                    result.push(newPositionedProduct);
                }
                resolve(result);
            }));
        });
    }
    getCustomersStatus(positionedProducts, path) {
        let productList = [];
        for (let positionedProduct of positionedProducts) {
            productList.push({
                "item": positionedProduct.product.productName,
                "ean": positionedProduct.product.ean,
                "node": positionedProduct.node.id
            });
        }
        let steps = [];
        for (let pathStep of path.nodes) {
            let eta = new Date(pathStep.eta);
            steps.push({
                "node": {
                    "ID": pathStep.node.id,
                    "Point": [pathStep.node.x, pathStep.node.y]
                },
                "eta": {
                    "date": {
                        "year": eta.getFullYear(),
                        "month": eta.getMonth(),
                        "day": eta.getDate()
                    },
                    "time": {
                        "hour": eta.getHours(),
                        "minute": eta.getMinutes(),
                        "second": eta.getSeconds(),
                        "nano": eta.getMilliseconds()
                    }
                },
                "duration": {
                    "seconds": pathStep.duration,
                    "nanos": 0
                }
            });
        }
        let json = {
            "shoppingList": productList,
            "estimatedPath": steps
        };
        return new Promise((resolve, reject) => {
            fetch(this.customerStatus_ServiceUrl, { method: 'POST', body: JSON.stringify(json) }).then((resRaw) => resRaw.text()).then((res) => {
                let result = new StoreStatus_1.default();
                //@ts-ignore
                for (let id in res['customers']) {
                    let shoppingList = [];
                    //@ts-ignore
                    for (let productRaw of res['customers'][id]['shoppingList']) {
                        let newProduct = new Product_1.default("", 0, productRaw['item'], productRaw['ean'], 0, false, "", 0, 0, new Category_1.default());
                        shoppingList.push(new PositionedProduct_1.default("", newProduct, new Node_1.default(productRaw['node'])));
                    }
                    let estimatedPath = new Path_1.default("");
                    //@ts-ignore
                    for (let obj of res['customers'][id]['estimatedPath']) {
                        estimatedPath.nodes.push(new PathStep_1.default("", 
                        //@ts-ignore
                        new Node_1.default(obj["node"]["ID"], obj["node"]["ID"]["Point"][0], obj["node"]["ID"]["Point"][1]), 
                        //@ts-ignore
                        new Date(obj["eta"]["date"]["year"], obj["eta"]["date"]["month"], obj["eta"]["date"]["day"], obj["eta"]["time"]["hour"], obj["eta"]["time"]["minute"], obj["eta"]["time"]["second"], obj["eta"]["time"]["nano"]), 
                        //@ts-ignore
                        parseFloat(obj["duration"]["seconds"])));
                    }
                    let newCustomerStatus = new CustomerStatus_1.default("", shoppingList, estimatedPath);
                    result.customers.push(newCustomerStatus);
                }
                resolve(result);
            });
        });
    }
    getMapInfoFromPath(bot, path) {
        return new Promise((resolve, reject) => {
            let json = {
                "UserId": bot.id,
                "Way": [],
                "UserPoint": {
                    x: bot.customerData.currentPosition.x, y: bot.customerData.currentPosition.y
                },
                "LastTime": new Date()
            };
            for (let step of path.nodes) {
                console.log(step);
                let stepAsJSON = {
                    "X": step.node.x,
                    "Y": step.node.y
                };
                json['Way'].push(stepAsJSON);
            }
            fetch(this.mapInfo_ServiceUrl + '/api/mapsetting', { method: 'POST', body: JSON.stringify(json) }).then((resRaw) => resRaw.text()).then((res) => {
                fetch(this.mapInfo_ServiceUrl + '/api/getmap?value=' + bot.id, { method: 'GET' }).then((responseRaw) => responseRaw.text()).then((response) => {
                    let result = new MapInfo_1.default(bot.map.id, response['Map']);
                    resolve(result);
                });
            });
        });
    }
    getNodeFromBot(bot) {
        return new Promise((resolve, reject) => {
            let json = {
                "id": bot.id
            };
            fetch(this.geofencing_ServiceUrl + '/api/', { method: 'POST', body: JSON.stringify(json) }).then((resRaw) => resRaw.text()).then((res) => {
                resolve(res);
            });
        });
    }
}
exports.MappingInterface = MappingInterface;
