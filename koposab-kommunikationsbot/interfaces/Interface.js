"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Interface {
    constructor() {
        this.interfaceUrl = "";
        this.mainThread = setInterval(() => { this.thread(); }, 100);
    }
    thread() {
    }
    sendRequest() {
    }
    handleResponse() {
    }
}
exports.Interface = Interface;
