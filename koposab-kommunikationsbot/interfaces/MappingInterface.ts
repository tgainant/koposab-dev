import { basename } from "path";
import MapInfo from "../models/MapInfo";
import StoreStatus from "../models/StoreStatus";
import Graph from "../models/Graph";
import AdditionalGraphInfo from "../models/AdditionalGraphInfo";
import CustomerData from "../models/CustomerData";
import PathRequest from "../models/PathRequest";
import PathResponse from "../models/PathResponse";
import MapInfoRequest from "../models/MapInfoRequest";
import MapInfoResponse from "../models/MapInfoResponse";
import Node from '../models/Node';

import Bot from "../models/Bot";
import { Interface } from "./Interface";
import PositionedProduct from "../models/PositionedProduct";
import ShoppingList from "../models/ShoppingList";
import Path from "../models/Path";
import PathStep from "../models/PathStep";
import { resolve } from "dns";
import CustomerStatus from "../models/CustomerStatus";
import Product from "../models/Product";
import Category from "../models/Category";

const fetch = require('node-fetch');
const FormData = require('form-data');
const fs = require('fs');
const path = require("path");

export class MappingInterface extends Interface{

  geofencing_ServiceUrl:String = "";
  positionedProduct_ServiceUrl:String = "https://koposab-nodeservice.azurewebsites.net/nodeservice/nodes_for_shoppinglist";
  customerStatus_ServiceUrl:String = "http://localhost:8080/rest";
  path_ServiceUrl:String = "http://localhost:8085/rest/mapf/coop/pathfinding";
  mapInfo_ServiceUrl:String = "";
  crypting_ServiceUrl:String = "";
  market_ServiceUrl:String = "";

    constructor(){
        super();
    }

    protected thread(){
      super.thread();
    }

    insertCustomer(supermarketId:number, customerStatus:CustomerStatus):Promise<any>{
      return new Promise<any>((resolve, reject) => {
        let productList:{}[] = [];

        for(let positionedProduct of customerStatus.shoppingList){
          productList.push({
            "item": positionedProduct.product.productName,
            "ean": positionedProduct.product.ean,
            "node": positionedProduct.node.id
          });
        }

        let steps:{}[] = [];

        for(let pathStep of customerStatus.estimatedPath.nodes){
          let eta = new Date(pathStep.eta);

          steps.push({
            "node": {
              "ID": pathStep.node.id,
              "Point": [pathStep.node.x, pathStep.node.y]
            },
            "eta": {
              "date": {
                "year": eta.getFullYear(),
                "month": eta.getMonth(),
                "day": eta.getDate()
              },
              "time": {
                "hour": eta.getHours(),
                "minute": eta.getMinutes(),
                "second": eta.getSeconds(),
                "nano": eta.getMilliseconds()
              }
            },
            "duration": {
              "seconds": pathStep.duration,
              "nanos": 0
            }
          });
        }

        let body = {
          "shoppingList": productList,
          "estimatedPath": steps
        };
        let url = this.customerStatus_ServiceUrl+"/supermarket/" + supermarketId + "/newcustomer/"+customerStatus.id;
        fetch(url, {method: 'PUT', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(body)}).then(
          (response: any) => {
            response.text();
          }
        ).then(
          (res:any) => {
            resolve(res);
          }
        ).catch((error:any) => {
          reject(error);
        });
      });
    }

    getStoreStatus(supermarketId:number):Promise<StoreStatus> {
      console.log(supermarketId);
      return new Promise<StoreStatus>((resolve, reject) => {  
        fetch(this.customerStatus_ServiceUrl+'/supermarket/'+supermarketId+'/status').then(
          (resRaw: any) => resRaw.text()
        ).then(
          (response:any) => {
            let res = JSON.parse(response);
            let result:StoreStatus = new StoreStatus();

            //@ts-ignore
            for(let id in res['customers']){
              let shoppingList:PositionedProduct[] = [];
              //@ts-ignore
              for(let productRaw of res['customers'][id]['shoppingList']){
                let newProduct = new Product("", 0, productRaw['item'], productRaw['ean'], 0, false, "", 0, 0, new Category());
                shoppingList.push(new PositionedProduct("", newProduct, new Node(productRaw['node'])));
              }

              let estimatedPath = new Path("");
              //@ts-ignore
              for(let obj of res['customers'][id]['estimatedPath']){
                estimatedPath.nodes.push(new PathStep(
                  "",
                  //@ts-ignore
                  new Node(obj["node"]["ID"], obj["node"]["Point"][0], obj["node"]["Point"][1]),
                  //@ts-ignore
                  new Date(obj["eta"]["date"]["year"], obj["eta"]["date"]["month"], obj["eta"]["date"]["day"], obj["eta"]["time"]["hour"], obj["eta"]["time"]["minute"], obj["eta"]["time"]["second"], obj["eta"]["time"]["nano"]),
                  //@ts-ignore
                  parseFloat(obj["duration"]["seconds"])
                  ));
              }

              let newCustomerStatus = new CustomerStatus(id, shoppingList, estimatedPath);
              result.customers.push(newCustomerStatus);
            }

            resolve(result);
          }
        ).catch(
          (error:any) => {
            reject(error);
          }
        );
      });
    }

  public getPath(graph:Graph, additionalGraphInfo:AdditionalGraphInfo, storeStatus:StoreStatus, customerData:CustomerData):Promise<Path>{
    let nodesList = [];
    let edgesList = [];
    let additionalNodesList = [];
    let additionalEdgesList = [];
    let obsoleteEdgesList = [];
    let customersList = {};
    let customerDataObj = {};

    for(let node of graph.nodes){
      let nodeAsJSON = {
        "ID": (parseInt(node.id) || 0),
        "Point": [node.x, node.y]
      };
      nodesList.push(nodeAsJSON);
    }

    for(let edge of graph.edges){
      let edgeAsJSON = {
        "ID": (parseInt(edge.id) || 0),
        "Source": (parseInt(edge.source.id) || 0),
        "Target": (parseInt(edge.target.id) || 0),
        "Weight": edge.weight
      };
      edgesList.push(edgeAsJSON);
    }

    for(let node of additionalGraphInfo.additionalNodes){
      let nodeAsJSON = {
        "ID": (parseInt(node.id) || 0),
        "Point": [node.x, node.y],
        "Type": node.type,
        "Name": node.name
      };
      additionalNodesList.push(nodeAsJSON);
    }

    for(let edge of additionalGraphInfo.additionalEdges){
      let edgeAsJSON = {
        "ID": (parseInt(edge.id) || 0),
        "Source": (parseInt(edge.source.id) || 0),
        "Target": (parseInt(edge.target.id) || 0),
        "Weight": edge.weight
      };
      additionalEdgesList.push(edgeAsJSON);
    }

    for(let edge of additionalGraphInfo.obsoleteEdges){
      let edgeAsJSON = {
        "ID": (parseInt(edge.id) || 0)
      };
      obsoleteEdgesList.push(edgeAsJSON);
    }

    for(let customer of storeStatus.customers){
      let shoppingList:{}[] = [];
      let estimatedPath:{}[] = [];

      for(let positionedProduct of customer.shoppingList){
        shoppingList.push({
          "item": positionedProduct.product.productName,
          "ean": positionedProduct.product.ean,
          "node": (parseInt(positionedProduct.node.id) || 0)
        });
      }

      for(let pathStep of customer.estimatedPath.nodes){
        let eta = new Date(pathStep.eta);

        estimatedPath.push({
          "node": {
            "ID": (parseInt(pathStep.node.id) || 0),
            "Point": [pathStep.node.x, pathStep.node.y]
          },
          "eta": {
            "date": {
              "year": eta.getFullYear(),
              "month": eta.getMonth(),
              "day": eta.getDate()
            },
            "time": {
              "hour": eta.getHours(),
              "minute": eta.getMinutes(),
              "second": eta.getSeconds(),
              "nano": eta.getMilliseconds()
            }
          },
          "duration": {
            "seconds": pathStep.duration,
            "nanos": 0
          }
        });
      }

      let customerAsJSON = {
        "shoppingList": shoppingList,
        "estimatedPath": estimatedPath
      };
      //@ts-ignore
      customersList[customer.id] = customerAsJSON;
    }

    let customerShoppingList:{}[] = [];
    for(let positionedProduct of customerData.shoppingList){
      customerShoppingList.push({
        "item": positionedProduct.product.productName,
        "ean": positionedProduct.product.ean,
        "node": (parseInt(positionedProduct.node.id) || 0)
      });
    }

    customerDataObj = {
      "shoppingList": customerShoppingList,
      "age": customerData.age,
      "ageAccuracy": customerData.ageAccuracy,
      "gender": customerData.gender
    };

    let json = {
      "Graph": {
        "Resolution":  [ graph.xResolution, graph.yResolution],
        "Nodes": nodesList,
        "Edges": edgesList
      },
      "AdditionalGraphInfo": {
        "AdditionalNodes": additionalNodesList,
        "AdditionalEdges": additionalEdgesList,
        "ObsoleteEdges": obsoleteEdgesList
      },
      "StoreStatus": {
        "customers": customersList
      },
      "Customer": customerDataObj
    };

    return new Promise<Path>((resolve, reject) => {
      fetch(this.path_ServiceUrl, {method: 'POST', body: JSON.stringify(json)}).then(
        (resRaw: any) => resRaw.text()
      ).then((resStr:string) => {
          let res:{} = JSON.parse(resStr);
          let result:Path = new Path();

          //@ts-ignore
          for(let obj of res["estimatedPath"] as {}[]){
            result.nodes.push(new PathStep(
              "",
              //@ts-ignore
              new Node(obj["node"]["ID"], obj["node"]["Point"][0], obj["node"]["Point"][1]),
              //@ts-ignore
              new Date(obj["eta"]["date"]["year"], obj["eta"]["date"]["month"], obj["eta"]["date"]["day"], obj["eta"]["time"]["hour"], obj["eta"]["time"]["minute"], obj["eta"]["time"]["second"], obj["eta"]["time"]["nano"]),
              //@ts-ignore
              parseFloat(obj["duration"]["seconds"])
              )
            );
          }

          resolve(result);
        }
      );
    });
  }

  /* ======================================= */

    public getPositionedProductListFromShoppingList(shopppingList:ShoppingList):Promise<PositionedProduct[]>{
      //console.log(shopppingList.toJSON());
      return new Promise<PositionedProduct[]>((resolve, reject) => {
        let body = {
          "shoppingList": shopppingList.toJSON(true)
        };
        fetch(this.positionedProduct_ServiceUrl, {method: 'POST', body: JSON.stringify(body)}).then(
          (response: any) => response.text()
        ).then(
          ((rawJson:any) => {
            let rawResult:{} = JSON.parse(rawJson);
            let result:PositionedProduct[] = [];
            // @ts-ignore
            for(let raw of rawResult.positionedProducts){
              let newPositionedProduct = new PositionedProduct();
              newPositionedProduct.fromJSON(raw);
              result.push(newPositionedProduct);
            }
            resolve(result);
          })
        );
      });
    }

    public getCustomersStatus(positionedProducts:PositionedProduct[], path:Path):Promise<StoreStatus>{
      let productList:{}[] = [];

      for(let positionedProduct of positionedProducts){
        productList.push({
          "item": positionedProduct.product.productName,
          "ean": positionedProduct.product.ean,
          "node": positionedProduct.node.id
        });
      }

      let steps:{}[] = [];

      for(let pathStep of path.nodes){
        let eta = new Date(pathStep.eta);

        steps.push({
          "node": {
            "ID": pathStep.node.id,
            "Point": [pathStep.node.x, pathStep.node.y]
          },
          "eta": {
            "date": {
              "year": eta.getFullYear(),
              "month": eta.getMonth(),
              "day": eta.getDate()
            },
            "time": {
              "hour": eta.getHours(),
              "minute": eta.getMinutes(),
              "second": eta.getSeconds(),
              "nano": eta.getMilliseconds()
            }
          },
          "duration": {
            "seconds": pathStep.duration,
            "nanos": 0
          }
        });
      }

      let json = {
        "shoppingList": productList,
        "estimatedPath": steps
      };

      return new Promise<StoreStatus>((resolve, reject) => {
        fetch(this.customerStatus_ServiceUrl, {method: 'POST', body: JSON.stringify(json)}).then(
          (resRaw: any) => resRaw.text()
        ).then((res:{}) => {
            let result:StoreStatus = new StoreStatus();

            //@ts-ignore
            for(let id in res['customers']){
              let shoppingList:PositionedProduct[] = [];
              //@ts-ignore
              for(let productRaw of res['customers'][id]['shoppingList']){
                let newProduct = new Product("", 0, productRaw['item'], productRaw['ean'], 0, false, "", 0, 0, new Category());
                shoppingList.push(new PositionedProduct("", newProduct, new Node(productRaw['node'])));
              }

              let estimatedPath = new Path("");
              //@ts-ignore
              for(let obj of res['customers'][id]['estimatedPath']){
                estimatedPath.nodes.push(new PathStep(
                  "",
                  //@ts-ignore
                  new Node(obj["node"]["ID"], obj["node"]["ID"]["Point"][0], obj["node"]["ID"]["Point"][1]),
                  //@ts-ignore
                  new Date(obj["eta"]["date"]["year"], obj["eta"]["date"]["month"], obj["eta"]["date"]["day"], obj["eta"]["time"]["hour"], obj["eta"]["time"]["minute"], obj["eta"]["time"]["second"], obj["eta"]["time"]["nano"]),
                  //@ts-ignore
                  parseFloat(obj["duration"]["seconds"])
                  ));
              }

              let newCustomerStatus = new CustomerStatus("", shoppingList, estimatedPath);
              result.customers.push(newCustomerStatus);
            }

            resolve(result);
          }
        );
      });
    }

    

    public getMapInfoFromPath(bot:Bot, path:Path):Promise<MapInfo>{
      return new Promise<MapInfo>((resolve, reject) => {
        let json = {
          "UserId": bot.id,
          "Way": [] as Array<{}>,
          "UserPoint": {
            x: bot.customerData.currentPosition.x, y: bot.customerData.currentPosition.y
          },
          "LastTime": new Date()
        };
  
        for(let step of path.nodes){
          console.log(step);
          let stepAsJSON = {
            "X": step.node.x,
            "Y": step.node.y
          };
          json['Way'].push(stepAsJSON);
        }
  
        fetch(this.mapInfo_ServiceUrl+'/api/mapsetting', {method: 'POST', body: JSON.stringify(json)}).then(
          (resRaw: any) => resRaw.text()
        ).then((res:any) => {
            fetch(this.mapInfo_ServiceUrl+'/api/getmap?value='+bot.id, {method: 'GET'}).then(
              (responseRaw: any) => responseRaw.text()
            ).then(
              (response:any) => {
                let result:MapInfo = new MapInfo(
                  bot.map.id, response['Map']
                );
                resolve(result);
              }
            );
          }
        );
      });
    }

    public getNodeFromBot(bot:Bot):Promise<Bot>{
      return new Promise<Bot>((resolve, reject) => {
        let json = {
          "id": bot.id
        };
  
        fetch(this.geofencing_ServiceUrl+'/api/', {method: 'POST', body: JSON.stringify(json)}).then(
          (resRaw: any) => resRaw.text()
        ).then((res:any) => {
            resolve(res);
          }
        );
      });
    }
}