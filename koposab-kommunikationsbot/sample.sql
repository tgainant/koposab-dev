-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Erstellungszeit: 29. Okt 2019 um 10:32
-- Server-Version: 5.7.24
-- PHP-Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Datenbank: `koposabkobot`
--
CREATE DATABASE IF NOT EXISTS `koposabkobot` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `koposabkobot`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bots`
--

DROP TABLE IF EXISTS `bots`;
CREATE TABLE IF NOT EXISTS `bots` (
  `id` varchar(36) NOT NULL,
  `name` text NOT NULL,
  `firstName` text NOT NULL,
  `actionsID` text NOT NULL,
  `shoppingList` varchar(36) NOT NULL,
  `timestampIntro` text NOT NULL,
  `timestampLastAction` text NOT NULL,
  `map` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` varchar(36) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `categories`
--

INSERT INTO `categories` (`id`, `description`) VALUES
('02941111-5a60-4ae7-b433-abbe9046968c', 'Frische & Kühlung'),
('4c979652-4410-4ea6-bd5e-4aa08221e997', 'Süßes & Salziges'),
('6b55914b-4ad3-4b49-8eba-5d64867cf7c2', 'Getränke'),
('9c619db5-9f51-4a93-b6e2-4ffd37bd055e', 'Tiefkühl'),
('d916189c-1f6b-4542-bf28-667e442a3672', 'Nahrungsmittel'),
('e4e8ff6a-c66d-41c5-8829-986ae63bd815', 'Kaffee, Tee & Kakao'),
('ed23edec-f406-40a0-9b53-bcf0d0f9af04', 'Obst & Gemüse');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mapinfos`
--

DROP TABLE IF EXISTS `mapinfos`;
CREATE TABLE IF NOT EXISTS `mapinfos` (
  `id` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `positions`
--

DROP TABLE IF EXISTS `positions`;
CREATE TABLE IF NOT EXISTS `positions` (
  `id` varchar(36) NOT NULL,
  `tagID` text NOT NULL,
  `xCoordinate` float NOT NULL,
  `yCoordinate` float NOT NULL,
  `timestampPosition` text NOT NULL,
  `speed` float NOT NULL,
  `direction` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` varchar(36) NOT NULL,
  `position` int(11) NOT NULL,
  `productName` text NOT NULL,
  `ean` text NOT NULL,
  `quantity` float NOT NULL,
  `bought` tinyint(1) NOT NULL,
  `brand` text NOT NULL,
  `shelf` int(11) NOT NULL,
  `price` float NOT NULL,
  `category` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shoppinglists`
--

DROP TABLE IF EXISTS `shoppinglists`;
CREATE TABLE IF NOT EXISTS `shoppinglists` (
  `id` varchar(36) NOT NULL,
  `products` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shoppinglists_products`
--

DROP TABLE IF EXISTS `shoppinglists_products`;
CREATE TABLE IF NOT EXISTS `shoppinglists_products` (
  `id` varchar(36) NOT NULL,
  `shoppingLists` varchar(36) NOT NULL,
  `products` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(36) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `firstName` text NOT NULL,
  `lastName` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `firstName`, `lastName`) VALUES
('b204317d-ca50-4d78-85d0-27207ed5a599', 'jmueller@ics-group.eu', '1234', 'Jacques', 'Müller'),
('b975ab6b-2f28-4f24-8953-babf37de262b', 'jsmith@ics-group.eu', '0000', 'John', 'Smith'),
('756378f9-5c16-44f1-a6a7-5a14495c2229', 'mdelpech@ics-group.eu', 'password', 'Michel', 'Delpech');
COMMIT;