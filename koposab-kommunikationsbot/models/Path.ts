
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import PathStep from "./PathStep";


class Path extends BaseModel{
	public id:string;
	public nodes:PathStep[];


	constructor(id:string = "", nodes:PathStep[] = []){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.nodes = nodes;


    	this.dataTable = 'path';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'nodes': ['nodes', 'compoM', PathStep]		};
	}
}
export default Path;