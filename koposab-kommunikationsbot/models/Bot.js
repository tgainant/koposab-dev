"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const ShoppingList_1 = __importDefault(require("./ShoppingList"));
const MapInfo_1 = __importDefault(require("./MapInfo"));
const CustomerData_1 = __importDefault(require("./CustomerData"));
class Bot extends BaseModel_1.default {
    constructor(id = "", name = "", firstName = "", actionsID = "", shoppingList = new ShoppingList_1.default(), timestampIntro = "", timestampLastAction = "", map = new MapInfo_1.default(), customerData = new CustomerData_1.default()) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.name = name;
        this.firstName = firstName;
        this.actionsID = actionsID;
        this.shoppingList = shoppingList;
        this.timestampIntro = timestampIntro;
        this.timestampLastAction = timestampLastAction;
        this.map = map;
        this.customerData = customerData;
        this.dataTable = 'bot';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'name': ['name', typeof this.name],
            'firstName': ['firstName', typeof this.firstName],
            'actionsID': ['actionsID', typeof this.actionsID],
            'shoppingList': ['shoppingList', 'compo', ShoppingList_1.default],
            'timestampIntro': ['timestampIntro', typeof this.timestampIntro],
            'timestampLastAction': ['timestampLastAction', typeof this.timestampLastAction],
            'map': ['map', 'compo', MapInfo_1.default],
            'customerData': ['customerData', 'compo', CustomerData_1.default]
        };
    }
}
exports.default = Bot;
