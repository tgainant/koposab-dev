"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const PathStep_1 = __importDefault(require("./PathStep"));
class Path extends BaseModel_1.default {
    constructor(id = "", nodes = []) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.nodes = nodes;
        this.dataTable = 'path';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'nodes': ['nodes', 'compoM', PathStep_1.default]
        };
    }
}
exports.default = Path;
