
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import CustomerStatus from "./CustomerStatus";


class StoreStatus extends BaseModel{
	public id:string;
	public customers:CustomerStatus[];


	constructor(id:string = "", customers:CustomerStatus[] = []){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.customers = customers;


    	this.dataTable = 'storestatus';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'customers': ['customers', 'compoM', CustomerStatus]		};
	}
}
export default StoreStatus;