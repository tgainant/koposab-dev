
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Graph from "./Graph";
import AdditionalGraphInfo from "./AdditionalGraphInfo";
import StoreStatus from "./StoreStatus";
import CustomerData from "./CustomerData";


class PathRequest extends BaseModel{
	public id:string;
	public graph:Graph;
	public additionalGraphInfo:AdditionalGraphInfo;
	public storeStatus:StoreStatus;
	public customer:CustomerData;


	constructor(id:string = "", graph:Graph = new Graph(), additionalGraphInfo:AdditionalGraphInfo = new AdditionalGraphInfo(), storeStatus:StoreStatus = new StoreStatus(), customer:CustomerData = new CustomerData()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.graph = graph;
		this.additionalGraphInfo = additionalGraphInfo;
		this.storeStatus = storeStatus;
		this.customer = customer;


    	this.dataTable = 'pathrequest';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'graph': ['graph', 'compo', Graph], 
			'additionalGraphInfo': ['additionalGraphInfo', 'compo', AdditionalGraphInfo], 
			'storeStatus': ['storeStatus', 'compo', StoreStatus], 
			'customer': ['customer', 'compo', CustomerData]
		};
	}
}
export default PathRequest;