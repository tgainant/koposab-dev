"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Node_1 = __importDefault(require("./Node"));
class Edge extends BaseModel_1.default {
    constructor(id = "", source = new Node_1.default(), target = new Node_1.default(), weight = 0) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.source = source;
        this.target = target;
        this.weight = weight;
        this.dataTable = 'edge';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'source': ['source', 'compo', Node_1.default],
            'target': ['target', 'compo', Node_1.default],
            'weight': ['weight', typeof this.weight]
        };
    }
}
exports.default = Edge;
