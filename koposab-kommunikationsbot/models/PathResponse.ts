
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import PositionedProduct from "./PositionedProduct";


class PathResponse extends BaseModel{
	public id:string;
	public shoppingList:PositionedProduct[];
	public estimatedPath:PositionedProduct;


	constructor(id:string = "", shoppingList:PositionedProduct[] = [], estimatedPath:PositionedProduct = new PositionedProduct()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.shoppingList = shoppingList;
		this.estimatedPath = estimatedPath;


    	this.dataTable = 'pathresponse';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'shoppingList': ['shoppingList', 'compoM', PositionedProduct], 
			'estimatedPath': ['estimatedPath', 'compo', PositionedProduct]
		};
	}
}
export default PathResponse;