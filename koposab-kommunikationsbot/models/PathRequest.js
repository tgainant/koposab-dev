"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Graph_1 = __importDefault(require("./Graph"));
const AdditionalGraphInfo_1 = __importDefault(require("./AdditionalGraphInfo"));
const StoreStatus_1 = __importDefault(require("./StoreStatus"));
const CustomerData_1 = __importDefault(require("./CustomerData"));
class PathRequest extends BaseModel_1.default {
    constructor(id = "", graph = new Graph_1.default(), additionalGraphInfo = new AdditionalGraphInfo_1.default(), storeStatus = new StoreStatus_1.default(), customer = new CustomerData_1.default()) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.graph = graph;
        this.additionalGraphInfo = additionalGraphInfo;
        this.storeStatus = storeStatus;
        this.customer = customer;
        this.dataTable = 'pathrequest';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'graph': ['graph', 'compo', Graph_1.default],
            'additionalGraphInfo': ['additionalGraphInfo', 'compo', AdditionalGraphInfo_1.default],
            'storeStatus': ['storeStatus', 'compo', StoreStatus_1.default],
            'customer': ['customer', 'compo', CustomerData_1.default]
        };
    }
}
exports.default = PathRequest;
