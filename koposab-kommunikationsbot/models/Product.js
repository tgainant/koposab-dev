"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Category_1 = __importDefault(require("./Category"));
class Product extends BaseModel_1.default {
    constructor(id = "", position = 0, productName = "", ean = "", quantity = 0, bought = false, brand = "", shelf = 0, price = 0, category = new Category_1.default()) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.position = position;
        this.productName = productName;
        this.ean = ean;
        this.quantity = quantity;
        this.bought = bought;
        this.brand = brand;
        this.shelf = shelf;
        this.price = price;
        this.category = category;
        this.dataTable = 'product';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'position': ['position', typeof this.position],
            'productName': ['productName', typeof this.productName],
            'ean': ['ean', typeof this.ean],
            'quantity': ['quantity', typeof this.quantity],
            'bought': ['bought', typeof this.bought],
            'brand': ['brand', typeof this.brand],
            'shelf': ['shelf', typeof this.shelf],
            'price': ['price', typeof this.price],
            'category': ['category', 'compo', Category_1.default]
        };
    }
}
exports.default = Product;
