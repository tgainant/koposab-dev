"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Graph_1 = __importDefault(require("./Graph"));
const AdditionalGraphInfo_1 = __importDefault(require("./AdditionalGraphInfo"));
const PositionedProduct_1 = __importDefault(require("./PositionedProduct"));
class MapInfoRequest extends BaseModel_1.default {
    constructor(id = "", graph = new Graph_1.default(), additionalGraph = new AdditionalGraphInfo_1.default(), sortedShoppingList = []) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.graph = graph;
        this.additionalGraph = additionalGraph;
        this.sortedShoppingList = sortedShoppingList;
        this.dataTable = 'mapinforequest';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'graph': ['graph', 'compo', Graph_1.default],
            'additionalGraph': ['additionalGraph', 'compo', AdditionalGraphInfo_1.default],
            'sortedShoppingList': ['sortedShoppingList', 'compoM', PositionedProduct_1.default]
        };
    }
}
exports.default = MapInfoRequest;
