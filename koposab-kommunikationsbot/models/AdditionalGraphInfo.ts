
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Node from "./Node";
import Edge from "./Edge";


class AdditionalGraphInfo extends BaseModel{
	public id:string;
	public additionalNodes:Node[];
	public additionalEdges:Edge[];
	public obsoleteEdges:Edge[];


	constructor(id:string = "", additionalNodes:Node[] = [], additionalEdges:Edge[] = [], obsoleteEdges:Edge[] = []){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.additionalNodes = additionalNodes;
		this.additionalEdges = additionalEdges;
		this.obsoleteEdges = obsoleteEdges;


    	this.dataTable = 'additionalgraphinfo';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'additionalNodes': ['additionalNodes', 'compoM', Node], 
			'additionalEdges': ['additionalEdges', 'compoM', Edge], 
			'obsoleteEdges': ['obsoleteEdges', 'compoM', Edge]		};
	}
}
export default AdditionalGraphInfo;