
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";


class Node extends BaseModel{
	public id:string;
	public x:number;
	public y:number;
	public type:string;
	public name:string;


	constructor(id:string = "", x:number = 0, y:number = 0, type:string = "", name:string = ""){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

	this.x = x;
	this.y = y;
	this.type = type;
	this.name = name;


    	this.dataTable = 'node';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'x': ['x', typeof this.x], 
			'y': ['y', typeof this.y], 
			'type': ['type', typeof this.type], 
			'name': ['name', typeof this.name]
		};
	}
}
export default Node;