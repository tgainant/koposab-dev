
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import PositionedProduct from "./PositionedProduct";
import Path from "./Path";


class CustomerStatus extends BaseModel{
	public id:string;
	public shoppingList:PositionedProduct[];
	public estimatedPath:Path;


	constructor(id:string = "", shoppingList:PositionedProduct[] = [], estimatedPath:Path = new Path()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.shoppingList = shoppingList;
		this.estimatedPath = estimatedPath;


    	this.dataTable = 'customerstatus';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'shoppingList': ['shoppingList', 'compoM', PositionedProduct], 
			'estimatedPath': ['estimatedPath', 'compo', Path]
		};
	}
}
export default CustomerStatus;