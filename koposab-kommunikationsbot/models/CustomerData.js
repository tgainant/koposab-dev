"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const PositionedProduct_1 = __importDefault(require("./PositionedProduct"));
const Node_1 = __importDefault(require("./Node"));
class CustomerData extends BaseModel_1.default {
    constructor(id = "", shoppingList = [], age = 0, ageAccuracy = "", gender = "", allergies = "", currentPosition = new Node_1.default()) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.shoppingList = shoppingList;
        this.age = age;
        this.ageAccuracy = ageAccuracy;
        this.gender = gender;
        this.allergies = allergies;
        this.currentPosition = currentPosition;
        this.dataTable = 'customerdata';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'shoppingList': ['shoppingList', 'compoM', PositionedProduct_1.default],
            'age': ['age', typeof this.age],
            'ageAccuracy': ['ageAccuracy', typeof this.ageAccuracy],
            'gender': ['gender', typeof this.gender],
            'allergies': ['allergies', typeof this.allergies],
            'currentPosition': ['currentPosition', 'compo', Node_1.default]
        };
    }
}
exports.default = CustomerData;
