
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import MapInfo from "./MapInfo";


class MapInfoResponse extends BaseModel{
	public id:string;
	public mapInfo:MapInfo;


	constructor(id:string = "", mapInfo:MapInfo = new MapInfo()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.mapInfo = mapInfo;


    	this.dataTable = 'mapinforesponse';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'mapInfo': ['mapInfo', 'compo', MapInfo]
		};
	}
}
export default MapInfoResponse;