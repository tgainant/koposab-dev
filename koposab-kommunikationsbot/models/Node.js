"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
class Node extends BaseModel_1.default {
    constructor(id = "", x = 0, y = 0, type = "", name = "") {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.x = x;
        this.y = y;
        this.type = type;
        this.name = name;
        this.dataTable = 'node';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'x': ['x', typeof this.x],
            'y': ['y', typeof this.y],
            'type': ['type', typeof this.type],
            'name': ['name', typeof this.name]
        };
    }
}
exports.default = Node;
