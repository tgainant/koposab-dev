
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Node from "./Node";


class PathStep extends BaseModel{
	public id:string;
	public node:Node;
	public eta:string;
	public duration:number;


	constructor(id:string = "", node:Node = new Node(), eta:string = "", duration:number = 0){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.node = node;
	this.eta = eta;
	this.duration = duration;


    	this.dataTable = 'pathstep';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'node': ['node', 'compo', Node], 
			'eta': ['eta', typeof this.eta], 
			'duration': ['duration', typeof this.duration]
		};
	}
}
export default PathStep;