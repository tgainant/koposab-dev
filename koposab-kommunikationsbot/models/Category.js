"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
class Category extends BaseModel_1.default {
    constructor(id = "", description = "") {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.description = description;
        this.dataTable = 'category';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'description': ['description', typeof this.description]
        };
    }
}
exports.default = Category;
