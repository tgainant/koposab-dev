"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const CustomerStatus_1 = __importDefault(require("./CustomerStatus"));
class StoreStatus extends BaseModel_1.default {
    constructor(id = "", customers = []) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.customers = customers;
        this.dataTable = 'storestatus';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'customers': ['customers', 'compoM', CustomerStatus_1.default]
        };
    }
}
exports.default = StoreStatus;
