
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Node from "./Node";
import Edge from "./Edge";


class Graph extends BaseModel{
	public id:string;
	public xResolution:number;
	public yResolution:number;
	public nodes:Node[];
	public edges:Edge[];


	constructor(id:string = "", xResolution:number = 0, yResolution:number = 0, nodes:Node[] = [], edges:Edge[] = []){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

	this.xResolution = xResolution;
	this.yResolution = yResolution;
		this.nodes = nodes;
		this.edges = edges;


    	this.dataTable = 'graph';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'xResolution': ['xResolution', typeof this.xResolution], 
			'yResolution': ['yResolution', typeof this.yResolution], 
			'nodes': ['nodes', 'compoM', Node], 
			'edges': ['edges', 'compoM', Edge]		};
	}
}
export default Graph;