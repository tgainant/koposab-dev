"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Product_1 = __importDefault(require("./Product"));
class ShoppingList extends BaseModel_1.default {
    constructor(id = "", products = []) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.products = products;
        this.dataTable = 'shoppinglist';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'products': ['products', 'compoM', Product_1.default]
        };
    }
}
exports.default = ShoppingList;
