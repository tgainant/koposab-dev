"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid');
const BaseModel_1 = __importDefault(require("../models/BaseModel"));
class User extends BaseModel_1.default {
    constructor(id = "", username = "", password = "", firstName = "", lastName = "") {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid.v4();
        }
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dataTable = 'users';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'username': ['username', typeof this.username],
            'password': ['password', typeof this.password],
            'firstName': ['firstName', typeof this.firstName],
            'lastName': ['lastName', typeof this.lastName]
        };
    }
}
exports.default = User;
