
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Graph from "./Graph";
import AdditionalGraphInfo from "./AdditionalGraphInfo";
import PositionedProduct from "./PositionedProduct";


class MapInfoRequest extends BaseModel{
	public id:string;
	public graph:Graph;
	public additionalGraph:AdditionalGraphInfo;
	public sortedShoppingList:PositionedProduct[];


	constructor(id:string = "", graph:Graph = new Graph(), additionalGraph:AdditionalGraphInfo = new AdditionalGraphInfo(), sortedShoppingList:PositionedProduct[] = []){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.graph = graph;
		this.additionalGraph = additionalGraph;
		this.sortedShoppingList = sortedShoppingList;


    	this.dataTable = 'mapinforequest';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'graph': ['graph', 'compo', Graph], 
			'additionalGraph': ['additionalGraph', 'compo', AdditionalGraphInfo], 
			'sortedShoppingList': ['sortedShoppingList', 'compoM', PositionedProduct]		};
	}
}
export default MapInfoRequest;