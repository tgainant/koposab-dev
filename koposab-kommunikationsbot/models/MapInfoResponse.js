"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const MapInfo_1 = __importDefault(require("./MapInfo"));
class MapInfoResponse extends BaseModel_1.default {
    constructor(id = "", mapInfo = new MapInfo_1.default()) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.mapInfo = mapInfo;
        this.dataTable = 'mapinforesponse';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'mapInfo': ['mapInfo', 'compo', MapInfo_1.default]
        };
    }
}
exports.default = MapInfoResponse;
