
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import PositionedProduct from "./PositionedProduct";
import Node from "./Node";


class CustomerData extends BaseModel{
	public id:string;
	public shoppingList:PositionedProduct[];
	public age:number;
	public ageAccuracy:string;
	public gender:string;
	public allergies:string;
	public currentPosition:Node;


	constructor(id:string = "", shoppingList:PositionedProduct[] = [], age:number = 0, ageAccuracy:string = "", gender:string = "", allergies:string = "", currentPosition:Node = new Node()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.shoppingList = shoppingList;
	this.age = age;
	this.ageAccuracy = ageAccuracy;
	this.gender = gender;
	this.allergies = allergies;
		this.currentPosition = currentPosition;


    	this.dataTable = 'customerdata';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'shoppingList': ['shoppingList', 'compoM', PositionedProduct], 
			'age': ['age', typeof this.age], 
			'ageAccuracy': ['ageAccuracy', typeof this.ageAccuracy], 
			'gender': ['gender', typeof this.gender], 
			'allergies': ['allergies', typeof this.allergies], 
			'currentPosition': ['currentPosition', 'compo', Node]
		};
	}
}
export default CustomerData;