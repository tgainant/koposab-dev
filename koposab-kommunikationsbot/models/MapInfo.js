"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
class MapInfo extends BaseModel_1.default {
    constructor(id = "", image = "") {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.image = image;
        this.dataTable = 'mapinfo';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'image': ['image', typeof this.image]
        };
    }
}
exports.default = MapInfo;
