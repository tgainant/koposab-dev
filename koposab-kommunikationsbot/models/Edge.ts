
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Node from "./Node";


class Edge extends BaseModel{
	public id:string;
	public source:Node;
	public target:Node;
	public weight:number;


	constructor(id:string = "", source:Node = new Node(), target:Node = new Node(), weight:number = 0){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.source = source;
		this.target = target;
	this.weight = weight;


    	this.dataTable = 'edge';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'source': ['source', 'compo', Node], 
			'target': ['target', 'compo', Node], 
			'weight': ['weight', typeof this.weight]
		};
	}
}
export default Edge;