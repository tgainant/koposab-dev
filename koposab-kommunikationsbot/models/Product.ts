
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Category from "./Category";


class Product extends BaseModel{
	public id:string;
	public position:number;
	public productName:string;
	public ean:string;
	public quantity:number;
	public bought:boolean;
	public brand:string;
	public shelf:number;
	public price:number;
	public category:Category;


	constructor(id:string = "", position:number = 0, productName:string = "", ean:string = "", quantity:number = 0, bought:boolean = false, brand:string = "", shelf:number = 0, price:number = 0, category:Category = new Category()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

	this.position = position;
	this.productName = productName;
	this.ean = ean;
	this.quantity = quantity;
	this.bought = bought;
	this.brand = brand;
	this.shelf = shelf;
	this.price = price;
		this.category = category;


    	this.dataTable = 'product';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'position': ['position', typeof this.position], 
			'productName': ['productName', typeof this.productName], 
			'ean': ['ean', typeof this.ean], 
			'quantity': ['quantity', typeof this.quantity], 
			'bought': ['bought', typeof this.bought], 
			'brand': ['brand', typeof this.brand], 
			'shelf': ['shelf', typeof this.shelf], 
			'price': ['price', typeof this.price], 
			'category': ['category', 'compo', Category]
		};
	}
}
export default Product;