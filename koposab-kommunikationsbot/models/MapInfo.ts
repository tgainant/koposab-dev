
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";


class MapInfo extends BaseModel{
	public id:string;
	public image:string;


	constructor(id:string = "", image:string = ""){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

	this.image = image;


    	this.dataTable = 'mapinfo';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'image': ['image', typeof this.image]
		};
	}
}
export default MapInfo;