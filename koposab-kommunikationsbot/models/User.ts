// @ts-ignore
const uuid = require('../node_modules/uuid');
import BaseModel from "../models/BaseModel";

class User extends BaseModel{
    public username:string;
    public password:string;
    public firstName:string;
    public lastName:string;

    constructor(id:string = "", username:string = "", password:string = "", firstName:string = "", lastName:string = ""){
        super();

        this.id = id;
        if(this.id == null || this.id == ""){
            this.id = uuid.v4();
        }

        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;

        this.dataTable = 'users';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'username': ['username', typeof this.username],
            'password': ['password', typeof this.password],
            'firstName': ['firstName', typeof this.firstName],
            'lastName': ['lastName', typeof this.lastName]
        };
    }
}
export default User;