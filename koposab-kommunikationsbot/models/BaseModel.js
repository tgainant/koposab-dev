"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseModel {
    constructor() {
        //Every instance of a Model has an id unique to the whole database
        this.id = 'noid';
        //Name of the DB table on which to save or load an instance of this Model
        this.dataTable = '';
        /*
            Mapping how to save the Model in DB through ORM
            Format is:
            {
                'attributeName': ['field name of the attribute in database', 'type of the attribute'],
                'attributeName': ['field name of the attribute in database', 'type of the attribute'],
                ...
            }
            If an attribute is another model, 'type of attribute' should be 'compo' or 'aggr' depending on the relationship between the models.
            The 'field name' will point to a database table in this case.
        */
        this.dataMap = {};
    }
    initialize() {
    }
    toJSON(recursive) {
        let result = {};
        for (let key in this.dataMap) {
            if (recursive) {
                //Is array of objects
                // @ts-ignore
                if (Array.isArray(this[key])) {
                    let arrayAsJSON = [];
                    // @ts-ignore
                    for (let attrObj of this[key]) {
                        // @ts-ignore
                        arrayAsJSON.push(attrObj.toJSON(true));
                    }
                    // @ts-ignore
                    result[key] = arrayAsJSON;
                }
                //Is object
                // @ts-ignore
                else if (this[key] != null && this[key].id != null) {
                    // @ts-ignore
                    result[key] = this[key].toJSON(true);
                }
                //Is simple attribute
                else {
                    // @ts-ignore
                    result[key] = this[key];
                }
            }
            else {
                // @ts-ignore
                result[key] = this[key];
            }
        }
        return result;
    }
    fromJSON(json) {
        for (let key in json) {
            if (key != "dataTable" && key != "dataMap") {
                // @ts-ignore
                //console.log(key+": "+json[key]);
                // @ts-ignore
                if (json[key].id != null) {
                    let model;
                    //Detect model
                    model = this.getModelFromAttributeName(key);
                    // @ts-ignore
                    let modifiedInstance = new model();
                    // @ts-ignore
                    modifiedInstance.fromJSON(json[key]);
                    // @ts-ignore
                    this[key] = modifiedInstance;
                }
                else {
                    // @ts-ignore
                    this[key] = json[key];
                }
            }
        }
    }
    getModelFromAttributeName(attributeName) {
        let map = [null, null, null];
        // @ts-ignore
        map = this.dataMap[attributeName];
        //console.log(map);
        return map[2];
    }
}
exports.default = BaseModel;
