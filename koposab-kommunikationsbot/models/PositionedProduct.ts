
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Product from "./Product";
import Node from "./Node";


class PositionedProduct extends BaseModel{
	public id:string;
	public product:Product;
	public node:Node;


	constructor(id:string = "", product:Product = new Product(), node:Node = new Node()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.product = product;
		this.node = node;


    	this.dataTable = 'positionedproduct';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'product': ['product', 'compo', Product], 
			'node': ['node', 'compo', Node]
		};
	}
}
export default PositionedProduct;