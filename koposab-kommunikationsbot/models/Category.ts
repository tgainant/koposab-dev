
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";


class Category extends BaseModel{
	public id:string;
	public description:string;


	constructor(id:string = "", description:string = ""){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

	this.description = description;


    	this.dataTable = 'category';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'description': ['description', typeof this.description]
		};
	}
}
export default Category;