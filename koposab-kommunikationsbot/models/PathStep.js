"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Node_1 = __importDefault(require("./Node"));
class PathStep extends BaseModel_1.default {
    constructor(id = "", node = new Node_1.default(), eta = "", duration = 0) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.node = node;
        this.eta = eta;
        this.duration = duration;
        this.dataTable = 'pathstep';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'node': ['node', 'compo', Node_1.default],
            'eta': ['eta', typeof this.eta],
            'duration': ['duration', typeof this.duration]
        };
    }
}
exports.default = PathStep;
