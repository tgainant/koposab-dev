
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import ShoppingList from "./ShoppingList";
import MapInfo from "./MapInfo";
import CustomerData from "./CustomerData";


class Bot extends BaseModel{
	public id:string;
	public name:string;
	public firstName:string;
	public actionsID:string;
	public shoppingList:ShoppingList;
	public timestampIntro:string;
	public timestampLastAction:string;
	public map:MapInfo;
	public customerData:CustomerData;


	constructor(id:string = "", name:string = "", firstName:string = "", actionsID:string = "", shoppingList:ShoppingList = new ShoppingList(), timestampIntro:string = "", timestampLastAction:string = "", map:MapInfo = new MapInfo(), customerData:CustomerData = new CustomerData()){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

	this.name = name;
	this.firstName = firstName;
	this.actionsID = actionsID;
		this.shoppingList = shoppingList;
	this.timestampIntro = timestampIntro;
	this.timestampLastAction = timestampLastAction;
		this.map = map;
		this.customerData = customerData;


    	this.dataTable = 'bot';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'name': ['name', typeof this.name], 
			'firstName': ['firstName', typeof this.firstName], 
			'actionsID': ['actionsID', typeof this.actionsID], 
			'shoppingList': ['shoppingList', 'compo', ShoppingList], 
			'timestampIntro': ['timestampIntro', typeof this.timestampIntro], 
			'timestampLastAction': ['timestampLastAction', typeof this.timestampLastAction], 
			'map': ['map', 'compo', MapInfo], 
			'customerData': ['customerData', 'compo', CustomerData]
		};
	}
}
export default Bot;