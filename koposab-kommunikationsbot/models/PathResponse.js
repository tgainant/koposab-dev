"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const PositionedProduct_1 = __importDefault(require("./PositionedProduct"));
class PathResponse extends BaseModel_1.default {
    constructor(id = "", shoppingList = [], estimatedPath = new PositionedProduct_1.default()) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.shoppingList = shoppingList;
        this.estimatedPath = estimatedPath;
        this.dataTable = 'pathresponse';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'shoppingList': ['shoppingList', 'compoM', PositionedProduct_1.default],
            'estimatedPath': ['estimatedPath', 'compo', PositionedProduct_1.default]
        };
    }
}
exports.default = PathResponse;
