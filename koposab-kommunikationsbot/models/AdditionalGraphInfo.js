"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Node_1 = __importDefault(require("./Node"));
const Edge_1 = __importDefault(require("./Edge"));
class AdditionalGraphInfo extends BaseModel_1.default {
    constructor(id = "", additionalNodes = [], additionalEdges = [], obsoleteEdges = []) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.additionalNodes = additionalNodes;
        this.additionalEdges = additionalEdges;
        this.obsoleteEdges = obsoleteEdges;
        this.dataTable = 'additionalgraphinfo';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'additionalNodes': ['additionalNodes', 'compoM', Node_1.default],
            'additionalEdges': ['additionalEdges', 'compoM', Edge_1.default],
            'obsoleteEdges': ['obsoleteEdges', 'compoM', Edge_1.default]
        };
    }
}
exports.default = AdditionalGraphInfo;
