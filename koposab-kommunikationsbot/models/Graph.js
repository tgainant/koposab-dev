"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Node_1 = __importDefault(require("./Node"));
const Edge_1 = __importDefault(require("./Edge"));
class Graph extends BaseModel_1.default {
    constructor(id = "", xResolution = 0, yResolution = 0, nodes = [], edges = []) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.xResolution = xResolution;
        this.yResolution = yResolution;
        this.nodes = nodes;
        this.edges = edges;
        this.dataTable = 'graph';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'xResolution': ['xResolution', typeof this.xResolution],
            'yResolution': ['yResolution', typeof this.yResolution],
            'nodes': ['nodes', 'compoM', Node_1.default],
            'edges': ['edges', 'compoM', Edge_1.default]
        };
    }
}
exports.default = Graph;
