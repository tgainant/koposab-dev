"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
const BaseModel_1 = __importDefault(require("./BaseModel"));
const Product_1 = __importDefault(require("./Product"));
const Node_1 = __importDefault(require("./Node"));
class PositionedProduct extends BaseModel_1.default {
    constructor(id = "", product = new Product_1.default(), node = new Node_1.default()) {
        super();
        this.id = id;
        if (this.id == null || this.id == "") {
            this.id = uuid();
        }
        this.product = product;
        this.node = node;
        this.dataTable = 'positionedproduct';
        this.dataMap = {
            'id': ['id', typeof this.id],
            'product': ['product', 'compo', Product_1.default],
            'node': ['node', 'compo', Node_1.default]
        };
    }
}
exports.default = PositionedProduct;
