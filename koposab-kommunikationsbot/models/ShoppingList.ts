
// @ts-ignore
const uuid = require('../node_modules/uuid/v4');
import BaseModel from "./BaseModel";
import Product from "./Product";


class ShoppingList extends BaseModel{
	public id:string;
	public products:Product[];


	constructor(id:string = "", products:Product[] = []){
    	super();

		this.id = id;
		if(this.id == null || this.id == ""){
			this.id = uuid();
 		}

		this.products = products;


    	this.dataTable = 'shoppinglist';
    	this.dataMap = {
			'id': ['id', typeof this.id], 
			'products': ['products', 'compoM', Product]		};
	}
}
export default ShoppingList;