"use strict";
const jest = require('../node_modules/jest');
jest.test('adds 1 + 2 to equal 3', () => {
    jest.expect(() => { return 1 + 2; }).toBe(3);
});
