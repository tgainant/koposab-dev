const fs = require('fs');
const mysql = require('mysql') ;
const mssql = require('mssql') ;
import BaseModel from "./models/BaseModel";
import User from './models/User';
import Debugger from "./Debugger";
import uuid = require("uuid");

class Mapper{
    client:string = 'mysql';//mssql for MicrosoftSQL, mysql for MySQL, mem for in memory
    host:string = '';
    port:string = '';
    user:string = '';
    password:string = '';
    database: string = '';

    models:Array<BaseModel> = [];

    knex:any;
    connection:any;

    instances:{} = {};//Used for in memory saving

    debugger:Debugger;

    initializeCallback:any = null;

    constructor(client:string, host:string, port:string, user:string, password:string, database:string){
        var that = this;

        this.client = client;
        this.host = host;
        this.port = port;
        this.user = user;
        this.password = password;
        this.database = database;

        this.debugger = new Debugger("");

        //Getting Models in the models folder
        fs.readdir('./models', (err:any, files:Array<any>) => {
            files.forEach(file => {
                if(file.endsWith(".js")){
                    this.models.push(<BaseModel>require('./models/'+file).default);
                }
            });

            if(that.client == "mem"){
                this.debugger.error("Relaunching mapper after detecting "+this.models.length+" model files...");
                that.initialize();
            }
        });
    }

    initialize(callback?:any){
        if(callback != null || callback != undefined){
            this.initializeCallback = callback;
        }

        this.debugger = require('./index').debugger;

        if(this.host != '' && this.user != '' && this.database != ''){
            let client = this.client;
            if(client == "mssql" || client == "mssql"){
                this.knex = require('knex')({
                    client: client,
                    connection: {
                        server : this.host,
                        options: {
                            port: +this.port
                        },
                        user : this.user,
                        password : this.password,
                        database : this.database
                    }
                });
            }
            else if(client == "mem"){
                this.debugger.info("Initializing use of in-memory mapper...");
                for(let model in this.models){
                    //@ts-ignore
                    this.instances[this.models[model].name] = [];
                }
                console.log(this.instances);
                console.log("In memory initialized.");
            }

            if(this.initializeCallback != null && callback == null){
                this.initializeCallback();
                this.initializeCallback = null;
            }
        }
    }

    query(query:string):Promise<any>{
        var that = this;

        that.debugger.info("== DATABASE QUERY ==");

        return new Promise((resolve, reject) => {
            switch(that.client){
                case'mysql':
                (async () => {
                    that.debugger.info("preparing querying...");
                    try {
                        if(this.connection == null){
                            let url = "mysql://server="+that.host+";databaseName="+that.database+";user="+that.user+";password="+that.password+";";
                            that.debugger.info("connecting to "+url+"...");
                            this.connection = mysql.createConnection({
                                'user': that.user,
                                'password': that.password,
                                'database': that.database,
                                'server': that.host,
                                'port': +that.port
                            });
                            //await mssql.connect(url);
                            await this.connection.connect();
                        }
                        that.debugger.info(query);
                        let result = this.connection.query(query, (error:any, results:any, fields:any) =>{
                            /*if(callback != null){
                                callback(results);
                            }*/
                            if(error != null){
                                reject(error);
                            }

                            resolve(results);
                            that.debugger.info("====");
                        });
                        //that.debugger.warning(result);

                        //this.connection.end();
                    } catch (err) {
                        that.debugger.error(err);
                        reject(err);
                    }
                })();
                break;
                case 'mssql':
                    (async () => {
                        try {
                            if(this.connection == null){
                                that.debugger.info("Starting Microsoft SQL connection to database");
                                //this.connection = await mssql.connect("mssql://"+that.user+":"+that.password+"@"+that.host+"/"+that.database);
                                const pool = new mssql.ConnectionPool({
                                    user: that.user,
                                    password: that.password,
                                    server: that.host,
                                    database: that.database
                                });

                                this.connection = await pool.connect();
                            }
                            that.debugger.info(query);
                            let result = await mssql.query(query);
                            resolve(result);
                        } catch (err) {
                            that.debugger.error(err);
                            reject(err);
                        }
                    })();

                    /*(async () => {
                        console.log("preparing querying...");
                        try {
                            let url = "jdbc:sqlserver://server="+that.host+";databaseName="+that.database+";user="+that.user+";password="+that.password+";";
                            console.log("connecting to "+url+"...");
                            let connectionConfig = {
                                'user': that.user,
                                'password': that.password,
                                'database': that.database,
                                'server': that.host,
                                'port': +that.port
                            };
                            //await mssql.connect(url);
                            await mssql.connect(connectionConfig);
                            console.log(query);
                            let result = await mssql.query(query);
                            that.debugger.warning(result);

                            if(callback != null){
                                callback(result);
                            }
                        } catch (err) {
                            that.debugger.error(err);
                        }
                    })();*/
                break;
            }
        });
    }

    /*saveInstance(instance:BaseModel):Promise<BaseModel>{
        var that = this;
        return new Promise((resolve, reject) => {

        });
    }*/

    saveInstance(instance:BaseModel, parentInstance?:BaseModel, parentAttribute?:string):Promise<BaseModel>{
        var that = this;

        let attributes: {} = instance.toJSON();

        console.log("saveInstance");
        console.log(attributes);

        if(that.client == "mem"){
            console.log("mem");
            console.log(instance);
            return new Promise((resolve, reject) => {
                for(let key in attributes){
                    //@ts-ignore
                    let attribute = instance[key];

                    //Check if JSON or Object
                    if(attribute.id != null){
                        let model:any;
                        //Detect model
                        model = instance.getModelFromAttributeName(key);

                        let renameInstance:BaseModel = new model();
                        //renameInstance.id = attribute.id;
                        renameInstance.fromJSON(attribute);

                        if(attribute.id == null || attribute.id == ""){
                            renameInstance.id = uuid();
                        }

                        that.saveInstance(renameInstance);

                        //@ts-ignore
                        instance[key] = renameInstance;
                    }
                    //Multiple relationship
                    else if(Array.isArray(attribute) && attribute.length > 0){
                        let model:any;
                        //Detect model
                        model = instance.getModelFromAttributeName(key);

                        for(let index in attribute){
                            let renameInstance:BaseModel = new model();
                            //renameInstance.id = attribute.id;
                            renameInstance.fromJSON(attribute[index]);

                            if(attribute[index].id == null || attribute[index].id == ""){
                                renameInstance.id = uuid();
                            }

                            that.saveInstance(renameInstance);

                            //@ts-ignore
                            instance[key][index] = renameInstance;
                        }
                    }
                }

                let indexExists:number = -1;

                //@ts-ignore
                for(let index in that.instances[instance.constructor.name]){
                    //@ts-ignore
                    let instanceInMemory = that.instances[instance.constructor.name];

                    if(instanceInMemory.id === instance.id){
                        indexExists = parseInt(index);
                        break;
                    }
                }

                if(indexExists > -1){
                    //@ts-ignore
                    that.instances[instance.constructor.name][indexExists] = instance;
                }
                else{
                    //@ts-ignore
                    that.instances[instance.constructor.name].push(instance);
                }

                //console.log(JSON.stringify(that.instances));

                that.loadInstance(instance.id).then((updatedInstance) => {
                    resolve(updatedInstance);
                }).catch((error) => {
                    that.debugger.error(error);
                    reject(error);
                });
            });
        }
        else{
            return new Promise((resolve, reject) => {
                let processedAttributes = 0;
                for(let key in attributes){
                    //@ts-ignore
                    let attribute = instance[key];

                    //Check if JSON or Object
                    if(attribute.id != null){
                        let model:any;
                        //Detect model
                        model = instance.getModelFromAttributeName(key);

                        let renameInstance:BaseModel = new model();
                        //renameInstance.id = attribute.id;
                        renameInstance.fromJSON(attribute);

                        if(attribute.id == null || attribute.id == ""){
                            renameInstance.id = uuid();
                        }

                        that.saveInstance(renameInstance);

                        //@ts-ignore
                        instance[key] = renameInstance.id;
                    }
                    //Multiple relationship
                    else if(Array.isArray(attribute) && attribute.length > 0){
                        let model:any;
                        //Detect model
                        model = instance.getModelFromAttributeName(key);

                        //@ts-ignore
                        instance[key] = "";

                        that.removeInstanceLinks(instance, key).then(() =>{
                            for(let index in attribute){
                                let renameInstance:BaseModel = new model();
                                renameInstance.fromJSON(attribute[index]);
                                if(attribute[index].id == null || attribute[index].id == ""){
                                    renameInstance.id = uuid();
                                }
                                that.saveInstance(renameInstance, instance, key).then((savedChildInstance) =>{
                                    //@ts-ignore
                                    instance[key][index] = savedChildInstance.id;
                                });
                            }
                            //@ts-ignore
                            //instance[key] = '';
                        }).catch((error) => {
                            that.debugger.error(error);
                            reject(error);
                        });
                    }
                    else{
                        //console.log(attribute);
                    }

                    processedAttributes++;
                    if(processedAttributes >= Object.keys(attributes).length){
                        console.log("MODEL "+instance.constructor.name);
                        switch(this.client){
                            default:
                                //Query building
                                var select = this.knex.where('id', instance.id).select().from(instance.dataTable).timeout(10000, {/*cancel: true*/}).toString();
                
                                //Querying
                                this.query(select).then((result) => {
                                    if(result.length == 0 || instance.id == null || instance.id == ""){
                                        //TODO if instance does not exist in database, check if there is one with a different id but the same values and report it (can be a duplicate)
                                        that.debugger.info("Model "+instance.constructor.name+"#"+instance.id+"@"+instance.dataTable+" cannot be found, creating a new instance.");
                                        //TODO check if the attributes are correctly typed, according to the dataMap, throw error if not
                                        that.createInstance(instance).then(function(queryResult:any){
                                            if(parentInstance != null && parentAttribute != null){
                                                that.createInstanceLink(parentInstance.dataTable, parentInstance.id, parentAttribute, queryResult.id).then(() => {
                                                    that.debugger.info("Done creating link between "+parentInstance.id+" and "+queryResult.id);
                                                });
                                            }

                                            that.loadInstance(queryResult.id).then((savedInstance) => {
                                                resolve(savedInstance);
                                            }).catch((error) => {
                                                that.debugger.error(error);
                                                reject(error);
                                            });
                                        }).catch((error) => {
                                            that.debugger.error(error);
                                            reject(error);
                                        });
                                    }
                                    else if(result.length == 1){
                                        //TODO check if the attributes are correctly typed, according to the dataMap
                                        that.updateInstance(instance).then(function(queryResult:any){
                                            if(parentInstance != null && parentAttribute != null){
                                                that.createInstanceLink(parentInstance.dataTable, parentInstance.id, parentAttribute, queryResult.id).then(() => {
                                                    that.debugger.info("Done updating link between "+parentInstance.id+" and "+queryResult.id);
                                                });
                                            }

                                            that.loadInstance(queryResult.id).then((updatedInstance) => {
                                                resolve(updatedInstance);
                                            }).catch((error) => {
                                                that.debugger.error(error);
                                                reject(error);
                                            });
                                        }).catch((error) => {
                                            that.debugger.error(error);
                                            reject(error);
                                        });
                                    }
                                    else if(result.length > 1){
                                        that.debugger.error("Multiple instances found for Model "+instance.constructor.name+"#"+instance.id+"@"+instance.dataTable+".");
                                    }
                                    else{
                                        //TODO?
                                    }
                                }).catch((error) => {
                                    that.debugger.error(error);
                                    reject(error);
                                });
                            break;
                        }
                    }
                }
            });
        }
    }

    createInstance(instance:BaseModel):Promise<BaseModel>{
        var that = this;
        
        return new Promise((resolve, reject) => {
            //Query building
            let insert:string = that.knex(instance.dataTable).insert(that.getInstanceAttributes(instance)).toString();

            //Querying
            if(insert != ""){
                switch(that.client){
                    default:
                    //Querying
                    that.query(insert).then((result) => {
                        if(result.affectedRows == 1){
                            console.log("createInstance");
                            console.log(instance);
                            resolve(instance);
                        }
                        else{
                            let error = "No affected rows while creating instance.";
                            that.debugger.error(error);
                            reject(error);
                        }                        
                    }).catch((error) => {
                        that.debugger.error(error);
                        reject(error);
                    });
                    break;
                }
            }
        });
    }

    createInstanceLink(parentTable:string, parentId:string, attributeName:string, childId:string):Promise<{}>{
        var that = this;
        
        return new Promise((resolve, reject) => {
            let newId = uuid();
            //Query building
            let insert:string = that.knex(parentTable+"_"+attributeName).insert({
                'id': newId,
                [''+parentTable]: parentId,
                [''+attributeName]: childId

            }).toString();

            //Querying
            if(insert != ""){
                switch(that.client){
                    default:
                    //Querying
                    that.query(insert).then((result) => {
                        if(result.affectedRows == 1){
                            let result = {
                                'id': newId,
                                [''+parentTable]: parentId,
                                [''+attributeName]: childId
                            }
                            resolve(result);
                        }
                        else{
                            let error = "No affected rows while creating link between these instances.";
                            that.debugger.error(error);
                            reject(error);
                        }                        
                    }).catch((error) => {
                        that.debugger.error(error);
                        reject(error);
                    });
                    break;
                }
            }
        });
    }

    updateInstance(instance:BaseModel):Promise<BaseModel>{
        var that = this;

        return new Promise((resolve, reject) => {
            let update = that.knex(instance.dataTable).where('id', instance.id).update(that.getInstanceAttributes(instance)).toString();

            //Querying
            if(update != ""){
                switch(that.client){
                    default:
                    //Querying
                    that.query(update).then((result) => {
                        if(result.affectedRows == 1){
                            resolve(instance);
                        }
                        else{
                            let error = "No affected rows while updating instance.";
                            that.debugger.error(error);
                            reject(error);
                        } 
                    }).catch((error) => {
                        that.debugger.error(error);
                        reject(error);
                    });
                    break;
                }
            }
        });
    }
    
    removeInstanceLinks(instance:BaseModel, attributeName:string):Promise<{}>{
        var that = this;

        if(instance.dataMap[attributeName][1] == "compoM"){
            // @ts-ignore
            let childInstances:Array<{}> = instance[attributeName] as Array<{}>;
            for(let key in childInstances){
                // @ts-ignore
                that.deleteInstance(childInstances[key]);
            }
        }
        
        return new Promise((resolve, reject) => {
            let removeAll = that.knex(instance.dataTable+"_"+attributeName).where({
                [''+instance.dataTable]: instance.id
            }).delete().toString();

            //Querying
            if(removeAll != ""){
                switch(that.client){
                    default:
                    //Querying
                    that.query(removeAll).then((result) => {
                        resolve();
                    }).catch((error) => {
                        that.debugger.error(error);
                        reject(error);
                    });
                    break;
                }
            }
        });
    }

    deleteInstance(instance:BaseModel):Promise<BaseModel>{
        var that = this;

        return new Promise((resolve, reject) => {
            for(let key in instance.dataMap){
                if(instance.dataMap[key][1] == "compo"){
                    //Check if we only have an id inside the attributes, not an object
                    // @ts-ignore
                    let attribute = instance[key];
                    let attributeId:string;
                    // Attribute is the id to a nested instance with a composition relation
                    if(attribute != null && attribute.id == null){
                        attributeId = attribute;
                    }
                    //Attribute is nested instance with a composition relation
                    else{
                        attributeId = attribute.id;
                    }

                    that.loadInstance(attributeId).then((result) => {
                        that.deleteInstance(result);
                    }).catch((error) => {
                        that.debugger.error(error);
                        reject(error);
                    });
                }
                else if(instance.dataMap[key][1] == "compoM"){
                    //Check if we only have an id inside the attributes, not an object
                    // @ts-ignore
                    let attribute = instance[key];
                    for(let attributeKey in attribute){
                        let attributeId:string;
                        // Attribute is the id to a nested instance with a composition relation
                        if(attribute[attributeKey] != null && attribute[attributeKey].id == null){
                            attributeId = attribute[attributeKey];
                        }
                        //Attribute is nested instance with a composition relation
                        else{
                            attributeId = attribute[attributeKey].id;
                        }

                        that.loadInstance(attributeId).then((result) => {
                            that.deleteInstance(result);
                        }).catch((error) => {
                            that.debugger.error(error);
                            reject(error);
                        });
                    }
                }
            }

            if(that.client == "mem"){
                //@ts-ignore
                for(let instancesIndex in that.instances){
                    //@ts-ignore
                    let instances = that.instances[instancesIndex];
                    for(let instancesCategoryIndex in instances){
                        let instanceInMemory = instances[instancesCategoryIndex];
                        if(instance.id == instanceInMemory.id){
                            //@ts-ignore
                            /*that.instances[instancesIndex] = */instances.splice(instancesCategoryIndex, 1);
                            let result = {
                                instance: instance,
                                affectedRows: 1
                            };
                            //@ts-ignore
                            resolve(result);
                            break;
                        }
                    }
                }
            }
            else{
                let deleteQuery = this.knex(instance.dataTable).where('id', instance.id).delete(this.getInstanceAttributes(instance)).toString();

                //Querying
                if(deleteQuery != ""){
                    switch(this.client){
                        default:
                        //Querying
                        this.query(deleteQuery).then((result) => {
                            resolve(result);
                        }).catch((error) => {
                            that.debugger.error(error);
                            reject(error);
                        });
                        break;
                    }
                }
            }
        });
    }

    loadInstance(id:string):Promise<BaseModel>{
        var that = this;

        return new Promise((resolve, reject) => {
            //var instance:BaseModel = new BaseModel();
            //instance.id = "";

            switch(this.client){
                default:
                    for(let key in this.models){
                        // @ts-ignore
                        let model = new this.models[key]();

                        if(model != null && model.dataTable != null && model.dataTable != ''){
                            //Query building
                            var select = this.knex.where('id', id).select().from(model.dataTable).timeout(10000).toString();

                            //Querying
                            this.query(select).then((result) => {
                                if(result.length == 1){
                                    //TODO check if the attributes are correctly typed, according to the dataMap
                                    let instance = model;
                                    this.setInstanceAttributes(instance, result[0]).then((modifiedInstance) => {
                                        resolve(modifiedInstance);
                                    });
                                }
                                else if(result.length > 1){
                                    let error = "Multiple instances found for #"+id+", cannot load a specific instance.";
                                    that.debugger.info(error);
                                    reject(error);
                                }
                                else{
                                    //Skip to next model and querying its dedicated table
                                }
                            }).catch((error) => {
                                that.debugger.error(error);
                            });
                        }
                    }
                break;
                case "mem":
                    for(let modelName in that.instances){
                        //@ts-ignore
                        for(let index in that.instances[modelName]){
                            //@ts-ignore
                            if(that.instances[modelName][index].id === id){
                                //@ts-ignore
                                resolve(that.instances[modelName][index]);
                            }
                        }
                    }
                    reject();
                    break;
            }
        });
    }

    /*
        Gets an array of instances for a specific attribute with a multiple objects relationship (composition or aggregation with X, n)
    */
   private loadMultipleInstances(rootInstance:BaseModel, attributeName:string):Promise<Array<BaseModel>> {
        var that = this;

        let instances:Array<BaseModel> = [];

        return new Promise((resolve, reject) => {
            //Query building
            var select = this.knex.where(rootInstance.dataTable, rootInstance.id).select().from(rootInstance.dataTable + "_" + attributeName).timeout(10000, { /*cancel: true*/}).toString();
            //Querying
            this.query(select).then((result) => {
                if (result.length > 0) {
                    for (let key in result) {
                        //TODO check if the attributes are correctly typed, according to the dataMap
                        // @ts-ignore
                        let childInstance = new rootInstance.dataMap[attributeName][2]();

                        this.loadInstance(result[key][attributeName]).then((loadedInstance) => {
                            instances.push(loadedInstance);
                            if (parseInt(key) == result.length - 1) {
                                resolve(instances);
                            }
                        });
                    }
                }
                else {
                    resolve([]);
                }
            }).catch((error) => {
                that.debugger.error(error);
            });
        });
    }

    loadInstances(model:BaseModel):Promise<BaseModel[]>{
        var that = this;

        return new Promise((resolve, reject) => {
            switch(that.client){
                default:
                    //Query building
                    // @ts-ignore
                    var select = that.knex.select().from(new model().dataTable).timeout(10000).toString();

                    //Querying
                    that.query(select).then((result) => {
                        if(result.length >= 1){
                            //console.log(result);
                            let instances:BaseModel[] = [];
                            for(let key in result){
                                //TODO check if the attributes are correctly typed, according to the dataMap
                                // @ts-ignore
                                let instance = new model();
                                //console.log(instance);
                                //console.log(key);
                                //console.log(result[key]);
                                that.setInstanceAttributes(instance, result[key]).then((modifiedInstance) => {
                                    instances.push(modifiedInstance);

                                    //console.log(instances.length+"/"+result.length);
                                    //console.log(instances);

                                    if(instances.length == result.length){
                                        resolve(instances);
                                    }
                                });
                            }
                        }
                        else{
                            //TODO
                        }
                    }).catch((error) => {
                        that.debugger.error(error);
                    });
                break;
                case "mem":
                    let instances:BaseModel[] = [];

                    //@ts-ignore
                    let modelInstance = new model();

                    //@ts-ignore
                    if(this.instances[modelInstance.constructor.name].length > 0){
                        let number = 0;
                        //@ts-ignore
                        for(let index in this.instances[modelInstance.constructor.name]){
                            //@ts-ignore
                            let instance = this.instances[modelInstance.constructor.name][index];

                            that.loadInstance(instance.id).then((loadedInstance) =>{
                                instances.push(loadedInstance);

                                number++;
                                //@ts-ignore
                                if(number >= this.instances[modelInstance.constructor.name].length){
                                    resolve(instances);
                                }
                            }).catch((error) => {
                                that.debugger.error(error);
                                reject(error);
                            });
                        }
                    }
                    else{
                        resolve(instances);
                    }
                    break;
            }
        });
    }

    /*
    Returns a json object with every attributes of a Model
    Format returned is {
        "attributeNameA": attributeValueA //Normal attribute
        "attributeNameB": {
            "attributeNameC": attributeValueB,
            "attributeNameD": attributeValueC
        }, //Compo / Aggregation attribute
        "attributeNameE": [
            {
                "attributeNameF": attributeValueD,
                "attributeNameG": attributeValueE
            },
            {
                "attributeNameF": attributeValueF,
                "attributeNameG": attributeValueG
            },
        ] // Multiple composition / aggregation attribute
    }
    Format?
    "attributeNameA": attributeValueA //Normal attribute
        "attributeNameB": idA //Compo / Aggregation attribute
        "attributeNameE": [idB, idC] // Multiple composition / aggregation attribute
    */
    private getInstanceAttributes(instance:BaseModel){
        let result:any = {};

        for(let key in instance.dataMap){
            if(instance.dataMap[key][1] == "compo" || instance.dataMap[key][1] == "aggr"){
                // @ts-ignore
                let attributeValue = instance[key];
                result[key] = attributeValue;
            }
            else if (instance.dataMap[key][1] == "compoM" || instance.dataMap[key][1] == "aggrM") {
                // @ts-ignore
                let attributeValue = instance[key];
                result[key] = attributeValue;
            }
            else{
                //TODO remove ts-ignore
                // @ts-ignore
                let attributeValue = instance[key];
                result[instance.dataMap[key][0]] = attributeValue;
            }
        }

        return result;
    }

    private setInstanceAttributes(instance:BaseModel, attributes:any):Promise<BaseModel>{
        var that = this;

        return new Promise((resolve, reject) => {
            let attributesList:any;

            //Recursive call from a nested instance?
            if(attributes == null){
                attributesList = {};
                let keys = Object.keys(instance.dataMap);
                for(let key of keys){
                    // @ts-ignore
                    attributesList[key] = instance[key];
                }
            }
            //Normal (root) instance
            else{
                attributesList = attributes;
            }

            //Used to only resolve the Promise when every attributes of the instance are set one by one (setting is asynchronous)
            let attributesModified:number = 0;

            for(let key in attributesList){
                let attributeType:string;

                if(instance.dataMap[key] == null){
                    let error:string = "Could not find an entry in "+instance.constructor.name+" dataMap for attributes "+key;
                    that.debugger.error(error);
                    reject(error);
                    break;
                }
                else{
                    attributeType = instance.dataMap[key][1];
                }

                //Is this attribute a nested instance?
                if(attributeType == "compo" || attributeType == "aggr"){
                    let nestedInstanceId = attributesList[key];

                    if(attributesList[key].id == null){
                        nestedInstanceId = attributesList[key];
                    }
                    //Handles nested instance inside nested instance
                    else{
                        nestedInstanceId = attributesList[key].id
                    }

                    //TODO check if nested instance id is null, in case dataMap was wrong or if we are looking for a nested instance which does not exist in DB

                    that.loadInstance(nestedInstanceId).then((nestedInstance) => {
                        this.setInstanceAttributes(nestedInstance, null).then((modifiedNestedInstance) => {
                            // @ts-ignore
                            instance[key] = modifiedNestedInstance.toJSON();
                            attributesModified++;
                            if(attributesModified >= Object.keys(attributesList).length){
                                //wait for async nested loadInstance, send response when every attributes have been modified
                                resolve(instance);
                            }
                        });
                    }).catch((error) => {
                        that.debugger.error(error);
                    });
                }
                else if (attributeType == "compoM" || attributeType == "aggrM") {
                    that.loadMultipleInstances(instance, key).then((instances) => {
                        let instancesAsJson = [];
                        for (let instanceLoaded of instances) {
                            instancesAsJson.push(instanceLoaded.toJSON());
                        }

                        // @ts-ignore
                        instance[key] = instancesAsJson;
                        attributesModified++;
                        if (attributesModified >= Object.keys(attributesList).length) {
                            //wait for async nested loadInstance, send response when every attributes have been modified
                            resolve(instance);
                        }
                    }).catch((error) => {
                        that.debugger.error(error);
                    });
                }
                //Attribute of primitive type
                else{
                    // @ts-ignore
                    instance[key] = attributesList[key];
                    attributesModified++;
                    if(attributesModified >= Object.keys(attributesList).length){
                        //wait for async nested loadInstance, send response when every attributes have been modified
                        resolve(instance);
                    }
                }
            }
        });
    }
}
export default Mapper;