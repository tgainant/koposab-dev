This application is meant to put together the parts of the KoPoSaB project created by DFKI with the parts from ICS.

## Run the application

- Make sure you have the **latest node.js LTS installed**.

- Make sure you have the **JDK in a version equal or greater than 13 installed**.

- Start all the services of the application by using at the root of the directory:

        npm run dev
    
## Next goals

- Testing interfaces to services that are online (Mapping service, for instance)
- Agreggate all services as **fully implemented**
- Make the application as a container / K8 cluster / orchestrated containers instead of an npm package running JARs.