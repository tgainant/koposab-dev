--- Routengenerierungsservice ---

Usage:
	-mode c -uri <Base-URI>
	
Beispiel: 
	-mode c -uri http://localhost:8085/rest
	
Erklärung:
	Andere Modi außer c für Cooperative, stehen nicht/noch nicht zur Verfügung.
	Der Routengenerierungsservice startet einen Webservice unter der eingegebenen Adresse und wartet auf Client-Requests.
	In welcher Struktur die Kommunikation via Json stattfindet ist in Confluence beschrieben.
	(siehe https://abmsoft.atlassian.net/wiki/spaces/KOB/pages/1454538757/Schnittstellen+-+JSON+Beispiele)
	Wenn eine fehlerhafte Anfrage gesendet wird oder Ähnliches geschieht, sollte der Webservice trotzdem danach noch weitere Anfragen bearbeiten können, lediglich die fehlerhafte Anfrage scheitert. 
	Bis dato berechnet der Routengenerierungsservice die Route für einen Kunden ohne die Routen der anderen Kunden zu berücksichtigen. Diese werden lediglich aus dem Request eingelesen.
	
Requests:

	Post-Request an <Base-URI>/mapf/coop/pathfinding
		Anfrage zum berechnen eines neuen Pfades für einen Kunden anhand dessen Einkaufsliste und antwortet mit dem berechneten Pfad und der sortierten Einkaufsliste