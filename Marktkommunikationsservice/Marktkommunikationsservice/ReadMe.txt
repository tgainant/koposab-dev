--- Marktkommunikationsservice ---

Usage:
	-kns <Base-URI> -uri <Base-URI>


Beispiel:
	-kns http://localhost:8084/rest -uri http://localhost:8086/rest

Erklärung:
	Der Marktkommunikationsservice stellt die Verbindung zwischen dem Supermarkt und dem Kommunikationsservice dar. Aktuell steht das Mapping der Einkaufslistenartikel zu den zugehörigen Knoten zur Verfügung. Dies geschieht per Weiterleitung an den Knotenservice.
	Mit der Supermarkt ID wird unterschieden von welchen Markt die Produkte stammen und angefragt werden. Der Knotenservice liefert dann das entsprechende Mapping passend zum Supermarkt.
	Zudem werden die Graphdaten und Zusatzinformationen für Güdingen und das IRL bereitgestellt. Dies geschieht über den Ordner Graphs. Neue Graphdaten können einfach in den Ordner hinzugefügt werden. Die entsprechende Bezeichnung und Nummerierung muss dabei jedoch eingehalten werden damoit den neuen Daten eine MarktId zugeordnet werden kann.
	
	
	Supermarkt Ids:
	Güdingen -> 0
	IRL -> 1
	
	
Requests:
	POST-Request an <Base-URI>/supermarket/<supermarketId>/nodeservice
		Der Kommunikationsservice sendet eine Einkaufsliste eines spezifischen Supermarkts an den Marktkommunikationsservice und erhält eine aktualisierte Einkaufsliste mit Knoten.
		
	Get-Request an <Base-URI>/supermarket/<supermarketId>/graphinfo
		Der Kommunikationsservice sendet eine Anfrage mit der <SupermarktId> und erhält die Graphdaten passend zur <SupermarktId>.
		
	Get-Request an <Base-URI>/supermarket/<supermarketId>/addgraphinfo
		Der Kommunikationsservice sendet eine Anfrage mit der <SupermarktId> und erhält die zusätzlichen Graphdaten passend zur <SupermarktId>.
